Tornado web and application
------------------------------------------------------------------------

    tornado.web.RequestHandler 處理URL與route的Mapping和get與post的方法應用取得溝通。
    methods write(), finish(), flush()都只能在main thread中使用，若你要使用多重thread
    的話請善用 IOLoop.add_callback 確認回傳時main thread依然存在。
    
    若你需要支援更多method(get/head/post)，你必須自行在 RequestHandler 自行增加。
    
    
    tornado route demo 
    
    ::
    
        from tornado.web import RequestHandler, Application
        from tornado.httpserver import HTTPServer
        from tornado import ioloop
        class MainHandler(RequestHandler):
            def get(self):
                self.write("You requested the main page")

        class StoryHandler(RequestHandler):
            def get(self, story_id):
                self.write("You requested the story " + story_id)

        application = Application([
            (r"/", MainHandler),
            (r"/story/([0-9]+)", StoryHandler),
        ])

        if __name__ == '__main__':
            ws_app = Application()
            server = HTTPServer(ws_app)
            server.listen(8080)
            ioloop.IOLoop.instance().start()

tornado.httpserver — Non-blocking HTTP server
-------------------------------------------------------------------------

    建立一個 HTTP server，預設就有request callback，request HTTpRequest.write和HTTPRequest.finish methods
    
    .. code-block:: python
    
        from tornado import httpserver
        from tornado import ioloop

        def handle_request(request):
            message='You requested {}\n'.format(request.uri)
            request.write('HTTP/1.1 200 OK\r\nContent-Length:{}\r\n\r\n{}'.format(len(message), message))
            request.finish()
            
        http_server = httpserver.HTTPServer(handle_request)
        http_server.listen(8888)
        ioloop.IOLoop.instance().start()

    HTTPServer的SSL連線設定加入
    
    .. code-block:: python
    
        HTTPServer(applicaton, ssl_options={
            "certfile": os.path.join(data_dir, "mydomain.crt"),
            "keyfile": os.path.join(data_dir, "mydomain.key"),
        })


    
    .. code-block:: python
    
        from tornado import httpserver
        from tornado import ioloop
        from tornado.web import RequestHandler, Application

        class MainHandler(RequestHandler):
            def get(self):
                self.write("Hello, world")

        def handle_request(request):
            message='You requested {}\n'.format(request.uri)
            request.write('HTTP/1.1 200 OK\r\nContent-Length:{}\r\n\r\n{}'.format(len(message), message))
            request.finish()

        app = Application([('/', MainHandler)])
        server = httpserver.HTTPServer(app)
        server.listen(8888)
        ioloop.IOLoop.instance().start()


    web.Appliction.listen可以用另外一種方法設定：
        
    .. code-block:: python
    
        from tornado import httpserver
        from tornado import ioloop
        from tornado.web import RequestHandler, Application

        class MainHandler(RequestHandler):
            def get(self):
                self.write("Hello, world")

        def handle_request(request):
            message='You requested {}\n'.format(request.uri)
            request.write('HTTP/1.1 200 OK\r\nContent-Length:{}\r\n\r\n{}'.format(len(message), message))
            request.finish()

        app = Application([('/', MainHandler)])
        http_server = httpserver.HTTPServer(handle_request)
        server = httpserver.HTTPServer(app)
        server.bind(8888)
        ioloop.IOLoop.instance().start()
