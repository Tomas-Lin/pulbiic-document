特殊的HTML顯示
--------------------------------------------------------------------

文繞圖
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    
    <em>     italic type 
    
    <p>                  
    
    <pre>    block type  
    
漸層色
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    ::
        Chrome/safari4+
        
        /*由上到下*/
        background:-webkit-gradient(linear, left top, left bottom, from(#00475E), to(#007276));

        /*由左至右*/
        background:-webkit-gradient(linear, left top, right top, from(#00475E), to(#007276));

        /*在Chrome 10 之後語法可簡化為:*/
        background:webkit-linear-gradient(top, #00475E, #007276);
        
        Firefox 3.6+ 
        /*由上到下*/
        background:-moz-linear-gradient(top, #00475E, #007276);

        /*由左至右*/
        background:-moz-linear-gradient(left, #00475E, #007276);
        
        IE6~IE8 
        
        filter: progid: DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='#00475E', endColorstr='#007276'); 

陰影與圓角
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    ::
    
        CSS3已經定義了 border-radius (圓角)及 box-shadow (陰影)兩種屬性。
        border-radius: 15px;
        box-shadow: 10px 10px 20px #000;
        
        Firefox、Safari、Chrome也有自己的特殊語法，但新版軟體已經支元CSS3提供的上述語法。
        -moz-border-radius: 15px;
        -webkit-border-radius: 15px;
        -moz-box-shadow: 10px 10px 20px #000;
        -webkit-box-shadow: 10px 10px 20px #000;
