HTML5 Tutorial
-----------------

What is HTML?
^^^^^^^^^^^^^^^

    HTML是一種語言，在web瀏覽器上顯示資訊，也有其他類似的語言，(e.g : XML)但是目前最常用的還是HTML。

    HTML在負責兩台電腦之間的溝通(Server, Client)之間的溝通和資訊傳遞，總體來說Server是資訊的提供者，而Client是資訊的索求者。

    HTML5有些部份的Element和屬性規格已經移出，但是為了相容之前架構的網頁，所以還是對這些Element有支援。

What is DOM?
^^^^^^^^^^^^^

    DOM(Document Object Model)是給HTML和XML所使用的一組API。(??)

    html DEMO

    .. code-block :: html

        <!DOCTYPE html>
        <html lang=”en”>
                <head>
                        <meta charset=”utf-8”>
                        <title>Awesome HTML5 Webpage</title>
                        <meta name=”description” content=”An awesome HTML5 page YOU built from scratch!”>
                        <meta name=”author” content=”Udemy”>
                        <link rel=”stylesheet” href=”style.css”>
                </head>
                <body>
                        <div id=”wrapper”>
                                <header class=”main_headline”>
                                        <nav>
                                                <ul>
                                                        <li><a href=”#”>About</a></li>
                                                        <li><a href=”#”>Services</a></li>
                                                        <li><a href=”#”>Products</a></li>
                                                        <li><a href=”#”>Contact</a></li>
                                                </ul>
                                        </nav>
                                        <h1>Super Duper Awesome Headline! </h1>
                                </header>
                                <div id=”primary_content”>
                                        <section id=”left_column”>
                                                <h3>A Hitchhiker’s Guide!</h3>
                                                <p>To the Galaxy!</p>
                                        </section>

                                        <section id=”right_column”>
                                                <article>
                                                        <header>
                                                                <h3>The Answer to Life?</h3>
                                                                <p>Published: 30 August 2013</p>
                                                        </header>
                                                        <p>Douglas Adams, when asked: “What is the answer to life, universe and everything”, replied:</p>
                                                        <blockquote>”42”</blockquote>
                                                </article>
                                        </section>
                                </div>

                                <footer>
                                        <p>The Footer is where all useless info goes!</p>
                                </footer>
                        </div>
                </body>
        </html>


What is diiferent between HTML4 and HTML5?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    簡化的語法？

    標籤的語義化

    新的標籤的制定(為了語義化？)與部份標籤的移除

    全新的表單設計


Why do we use HTML5?
^^^^^^^^^^^^^^^^^^^^^

    HTML5和HTML4.01的Tag並不完全一樣，例如<font>這一個Tag在HTML5已經不再使用，
    但是在HTML4.01仍可以使用，

HTML5 Remove Tag
^^^^^^^^^^^^^^^^^^

    <big></big> || <b></b> || <font></font> || <center></center>

        標籤的功能都可以利用Css 實現，並且這些標籤並沒有明確的語意。

diff between HTML5 and HTML4
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    HTML4

    <link href="special.css" rel="stylesheet" type="text/css">

    HTML5

    <link rel=”stylesheet” href=”style.css”>


    HTML4

    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

    HTML5

    <!DOCTYPE html>

    HTML4

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    HTML5
    <meta charset="utf-8">


HTML5 New Tag
^^^^^^^^^^^^^^^

    <!DOCTYPE html>

        宣告此文件的程式碼為html5，必須在文件的第1行，前面不能添加任何空格或是Tab，


    <header></header>

        主要在語義化標籤之一，在HTML4中通常使用<div id="wrapper"></div>，內容主要是放置WebPage的Logo或是Title Logo。

    <nav></nav>

        語義化標籤之一，在HTML4中通常使用<div id="primary_content">，內容主要放置WebPage 的導覽列

    <section></section>

        語義化標籤之一，功能和div類似，內容放置必須和上下文有相關性質的內容。

    <article></article>

        語義化標籤之一，功能和div類似，內容放置和上下文沒有相關性質的內容，例如部落格文章或是論壇的帖子等。

    <footer></footer>

        語義化標籤之一，在HTML4中通常使用<div id="footer"></div>，內容主要是放置WebPage的個人資訊或是在頁尾的資料。

    <main></main>

        放置網頁中主要的內容，每一個document 只能容許一個<main></main>的存在

    <aside></aside>

        語義化標籤之一，主要再放置左邊或右邊的目錄列。

    <video src=""></video> || <audio src=""></audio>

        放置多媒體內容(影片，聲音檔...等等)，HTML有API的支援，

    <track></track>

        支援video，(還要test 沒玩過)

    <time></time>

        顯示時間，真實作用還要test

    <canvas></canvas>

        支援繪圖功能的tag，

    <address>......</address>

        網路位址或地址等資料，這個tag裡不能包含<article>, <aside>, <nav>, <section>,
        <header>, <footer>, <hgroup>, <h1>-<h6> 或其他的 <address> 標籤

    <area>...</area>

    <section></section>

        區塊標籤，有上下文連貫時使用。

    <article></article>

        單獨存在的區塊標籤，內容為獨立存在。

    <fieldset> form element</fieldset>

        form element grouping tag

    <legend></legend>

        <fieldset> 或是 <detail> <figure> 這三類型tag使用時，可以用<legend>標示這三個內容的標題

HTML5 Form Element
-------------------


參考資料：
----------------------------------

    HTML5 Tutorial : http://html-5-tutorial.com

    HTML Tag Tutorial : http://www.quackit.com/

    DOM explain in wiki :http://en.wikipedia.org/wiki/Document_Object_Model

    HTML5 Tutorial for Beginners: https://www.udemy.com/blog/html5-tutorial-for-beginners/


