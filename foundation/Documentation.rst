Documentation
---------------

Get Starting
""""""""""""""""


    Install the compass gem

        install ruby

        ::

            $ apt-get install ruby

        ::

            $ gem install zurb-foundation

        ::

            $ gem install compass


Get New Project
""""""""""""""""



DEMO custom.scss
"""""""""""""""""""""

    custom.scss DEMO

    ::

        #main p {
          color: #00ff00;
          width: 97%;

          .redbox {
            background-color: #ff0000;
            color: #000000;
          }
        }

    上述經過 compiler之後的結果為

    ::

        #main p {
          color: #00ff00;
          width: 97%; }
          #main p .redbox {
            background-color: #ff0000;
            color: #000000; }

    ::

        a {
          font-weight: bold;
          text-decoration: none;
          &:hover { text-decoration: underline; }
          body.firefox & { font-weight: normal; }
        }

    compiler後

    ::

        a {
          font-weight: bold;
          text-decoration: none; }
          a:hover {
            text-decoration: underline; }
          body.firefox a {
            font-weight: normal; }

""""""""""""""""

    官方網站 : http://foundation.zurb.com/old-docs/f3/compass.php
