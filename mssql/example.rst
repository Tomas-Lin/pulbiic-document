MsSQL Sample
-------------

Nodejs connect sample
^^^^^^^^^^^^^^^^^^^^^^

    base on : cruchbang system(debian)

    Node.js - use the latest version if possible, but it has been tested on node 0.10.26 and later

    node-mssql-connector(Version : 0.2.2) : https://github.com/Nachbarshund/node-mssql-connector

Install node-mssql-connector
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ::

        npm install node-mssql-connector

    Quickly Start

    ::

        MSSQLConnector = require( "node-mssql-connector" )
        MSSQLClient = new MSSQLConnector({
            settings: {
                max: 20,
                min: 0,
                idleTimeoutMillis: 30000
            },
            connection: {
                userName: "",
                password: "",
                server: "",
                options: {
                    database: ""
                }
            }
            });

        query = MSSQLClient.query( "Query");

        query.exec(function(err, res){
            if(err){
                 console.log('err'+ err);
                  return;
            }
                console.log(JSON.stringify(res));
            });


Python Connect sample
^^^^^^^^^^^^^^^^^^^^^^

    base on : cruchbang system(debian)

    tool : python-pyodbc(version 2.1.7)

    Microsoft OLE DB Provider for DB2 Version 4.0 : http://msdn.microsoft.com/en-us/library/hh598132(v=SQL.10).aspx

    Install MicrosoftDriverOnDebianLinux : http://code.google.com/p/odbc/wiki/InstallingMicrosoftDriverOnDebianLinux

    If install pyodbc with pip

    ::

        pip install --allow-unverified pyodbc pyodbc

    install python -pyodbc and FreeTDS : version 0.91

    ::

        sudo apt-get install python-pyodbc freetds-bin

    Edit /etc/odbcinst.ini

    ::

        [FreeTDS]
        Driver = /usr/local/lib/libtdsodbc.so
        Threading = 1

    Install setenv

    ::

         sudo apt-get install tcsh

    vim .bashrc

    ::

        #add this

        export TDSVER=8.0

    /etc/odbcinst.ini

    ::
        [ODBC]
        Trace = Yes
        TraceFile = /tmp/odbc.log

        [ODBC Driver 11 for SQL Server]
        Description     = Microsoft ODBC Driver 11 for SQL Server
        Driver      = /opt/microsoft/msodbcsql/lib64/libmsodbcsql-11.0.so.2270.0
        Threading       = 1
        UsageCount      = 2

        [FreeTDS]
        Description = TDS driver (Sybase/MS SQL)
        Driver      = /usr/local/lib/libtdsodbc.so
        #Setup = /usr/lib/i386-linux-gnu/odbc/libtdsS.so
        UsageCount = 1
        TDS Version = 8.0
        client charset = utf-8
        #Threading = 1

python sample code

    .. code-block :: python

        import pyodbc
        conn = pyodbc.connect('DRIVER={FreeTDS};Server=%s;UID=%s;PWD=%s;TDS_Version=8.0;Port=1433;' % ('192.168.11.139', 'sa', 'happy789!@#'))

參考資料：：

    TDS version Error : http://stackoverflow.com/questions/8890427/pyodbc-freetds-unixodbc-on-debian-linux-issues-with-tds-version

    TDS protocel version document : http://www.freetds.org/userguide/choosingtdsprotocol.htm

    tsql commond line example : http://stackoverflow.com/questions/6973371/freetds-problem-connecting-to-sql-server-on-mac-unexpected-eof-from-the-server

    前人測試狀態討論區 ： https://groups.google.com/forum/#!topic/python-cn/ID52GfOe9AY

    tsql example : http://www.haidongji.com/2006/08/31/connecting-to-sql-server-from-unix-or-linux-with-freetds/

    pip install pyodbc : https://github.com/pypa/pip/issues/1586

    freetds conf document : http://www.freetds.org/userguide/freetdsconf.htm

    FreeTDS Download : http://www.freetds.org/software.html

    libtdsodbc.so 找不到的處理方式 : http://lists.ibiblio.org/pipermail/freetds/2007q2/021191.html

    unixODBC 的參考文件 : http://www.dk101.com/Discuz/viewthread.php?tid=53447#.U37w93WSxRQ

    python link mssql driver download : http://www.microsoft.com/en-us/download/details.aspx?id=36437

    安裝mssql driver 的步驟 : http://code.google.com/p/odbc/wiki/InstallingMicrosoftDriverOnDebianLinux

    安裝mssql driver 的官方文件 : http://msdn.microsoft.com/en-us/library/hh873008(v=sql.110).aspx
    String example : http://stackoverflow.com/questions/16770729/unable-to-connect-to-database-pyodbc


