Sugar Trace Note 
-------------------------------------------------------

紀錄，暫時沒空整理！

很亂的美感！

筆記:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    sugar 是一個shell script，執行之後會用exec呼叫 sugar-session 這一個python檔案將視窗外框，聲音，整個利用GTK繪畫出來。

    /usr/share/sugar/activities/Hello.activity/activity/active.info
    
    sugar 目錄架構：
    
        ::
        
            XXXXX.activity/
                Insert.py
                setup.py
                activity/
                    activity.info
                    image.svg
                    
    
    setup.py
    
    ::
        
         #!/usr/bin/env python
         try:
             from sugar.activity import bundlebuilder
             bundlebuilder.start()
         except ImportError:
             import os
             os.system("find ./ | sed 's,^./,InsertActivity.activity/,g' > MANIFEST")
             os.system('rm InsertActivity.xo')
             os.chdir('..')
             os.system('zip -r InsertActivity.xo InsertActivity.activity')
             os.system('mv InsertActivity.xo ./InsertActivity.activity')
             os.chdir('InsertActivity.activity')
             
    對此APP進行設定
    activity.info
    
        ::
        
            [Activity]
            name = Insert_Demo
            bundle_id=org.laptop.Insert
            exec = sugar-activity Insert.InsertActivity
            icon = Test
            activity_version = 0.1
            show_launcher = yes
    
    Insert.py
    
    ::
        
        from sugar.activity import activity
        from pymongo_class import PyConnect
        import gtk

        class InsertActivity(activity.Activity):
            def __init__(self, handle):
                self.mongo = PyConnect('localhost', 27017)
                self.mongo.use('test')
                self.mongo.setCollection('test')
                self.__main_view = gtk.HBox()
                self.set_canvas(self.__main_view)
                toolbox = activity.ActivityToolbox(self)
                self.set_toolbox(toolbox)
                toolbox.show()
                self.button = gtk.Button("Insert")
                self.entry = gtk.Entry()
                self.__main_view.pack_start(self.entry)
                self.__main_view.pack_start(self.button)
                self.__main_view.show_all()
                self.entry.show()
                self.button.show()
                self.button.connect("clicked", self.insert, None)

            def insert(self, widget, data=None):
                text = self.entry.get_text()
                self.mongo.insert(text = text)
        
        
    activity/image.svg 是sugar在desktop上顯示的圖檔，只能夠使用svg檔案

Pygtk
^^^^^^^^^^^^^^^^^^^^^^^^^^^

    gtk.VBox和gtk.HBox繼承於gtk.Box。
    
    
    
    
classroompresents問題：
^^^^^^^^^^^^^^^^^^^^^^^^^^^

    classroompersent連線時：
        
        無法選擇開啟檔案
        
        開啟的並非同一個檔案，而是各自開啟各自的事件
    
        因為不是同一個檔案所以無法測試共筆功能
        
        無法正常儲存檔案
        
        傳送檔案時，另一端並無之選項，僅僅有傳送端可以取消傳送按鈕
        
        toolbar 上面修改筆劃粗細語言色無icon
        
        
