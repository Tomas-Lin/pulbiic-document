Node-webkit SDK
----------------------------------------------------------------

    ::
    
        $ git clone git://github.com/jsmarkus/node-webkit-sdk.git
        $ cd node-webkit-sdk
        $ ant sdk
        
    
    若要指定下載版本則 
    
    ::
        
        ant sdk -Dnw-version=版本號
        
    指定安裝檔路徑
    
    ::
    
        $ ant create -Dtemplate=default -Ddir='專案路徑'
        $ cd 專案路徑
        
    ::
    
        step 1.
            
            32bit : ant dist.l32
            64bit : ant dist.l64
            windows : ant dist.win
            all : ant dist.all
            
        step 2.
        
            將檔案放至於src(source)
            
        step 3.
        
            ant app.nw

        step 4.
        
            32bit : ant bin.l32
            64bit : ant bin.l64
            windows : ant bin.win
            all : ant dist.bin.all


參考網站
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    git : https://github.com/jsmarkus/node-webkit-sdk
