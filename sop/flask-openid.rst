flask OpenID
-------------------------------------------------------------------
    
    virtualenv 基本安裝與使用
    
    step 1.
        
        確認套件版本是否與下列相同
        
    step 2.
    
        pip install flask flask-login flask-openid pymongo
    
    step 3.
    
        python run.py
        
    step 4.
    
        打開瀏覽器輸入URL
        
        http://localhost:50000
        
支援openid 的網站
-----------------------------------------------------------------

    台灣：   Yahoo
    
            Google
              
    大陸：(未測試)   
    
            腾讯微博 ：http://dev.qq.com/wiki/index.php?title=OpenId
    
            QQ : http://haotushu.sinaapp.com/post-453.html
            
            百度帳號：http://tieba.baidu.com/f?kz=314849861
            
            
使用套件與技術
-------------------------------------------------------------------
    
    python v2.7
    
    pip v1.1
    
    flask v0.10
    
    flask-login v0.2.3

    pymongo v2.5.2
    
    flask OpenID v1.0

    mongo v2.4.4 :
    
        MONGO下載位置： 32bit : http://api.viglink.com/api/click?format=go&key=084c74521c465af0d8f08b63422103cc&loc=http%3A%2F%2Fwww.mongodb.org%2Fdownloads&v=1&libId=675f367e-4821-4517-acfb-e35cf3e792c6&out=http%3A%2F%2Ffastdl.mongodb.org%2Flinux%2Fmongodb-linux-i686-2.4.4.tgz&ref=http%3A%2F%2Fwww.mongodb.org%2F&title=Downloads%20-%20MongoDB&txt=download&jsonp=vglnk_jsonp_13724101752198
                    
                      64bit : http://api.viglink.com/api/click?format=go&key=084c74521c465af0d8f08b63422103cc&loc=http%3A%2F%2Fwww.mongodb.org%2Fdownloads&v=1&libId=675f367e-4821-4517-acfb-e35cf3e792c6&out=http%3A%2F%2Ffastdl.mongodb.org%2Flinux%2Fmongodb-linux-x86_64-2.4.4.tgz&ref=http%3A%2F%2Fwww.mongodb.org%2F&title=Downloads%20-%20MongoDB&txt=download&jsonp=vglnk_jsonp_13724102054259
                      
        MONGO 安裝步驟：http://docs.mongodb.org/manual/installation/
