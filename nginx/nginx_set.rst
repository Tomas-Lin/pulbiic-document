Nginx Set
---------------------------

安裝nginx
^^^^^^^^^^^^^^^^^^^^^^^^^^

    **Fedora**
    
    ::
    
        #sudo yum install nginx
        
    **debain**
    
    ::
    
        #sudo apt-get install nginx
        
    **Config Path**    
    ::
    
        # fedora Path:/etc/nginx/conf.d/
        
        # debain Path:/etc/nginx/sites-available/
        
    **set sample:**
    nginx:
    ::

        #
        # The default server
        #
        server {
            listen       80 default_server;
            server_name  _;
            
            charset utf-8;
            # 設定/目錄位置
            location / {
                root   /usr/share/nginx/html;
                index  index.html index.htm;
            }
            #/
            location /apache/ {
                proxy_pass http://www.tomasdemo.idv.tw:7000/;
            }
        
            location /flask/ {
                proxy_pass http://www.tomasdemo.idv.tw:5000/;
            }
            
            error_page  404              /404.html;
            location = /404.html {
                root   /usr/share/nginx/html;
            }
        
            # redirect server error pages to the static page /50x.html
            #
            error_page   500 502 503 504  /50x.html;
            location = /50x.html {
                root   /usr/share/nginx/html;
            }
      
            # deny access to .htaccess files, if Apache's document root
            # concurs with nginx's one
            #
            #location ~ /\.ht {
            #    deny  all;
            #}
        }

    apache:
    ::
    
        # Listen 7000
