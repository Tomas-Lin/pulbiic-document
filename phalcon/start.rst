Phalcon Start
---------------

Install
^^^^^^^^^

    Debian:

    ..note ::

        #Install necessart package
        sudo apt-get install git-core gcc autoconf
        sudo apt-get install php5-dev php5-mysql

        #Install MySQL database
        sudo apt-get install mysql-server-5.5

        mkdir ~/phalcon
        cd ~/phalcon
        git clone git://github.com/phalcon/cphalcon.git
        cd cphalcon/build
        sudo ./install

        #Add extension in php.ini
        extension=phalcon.so

        #restart the web server

        /etc/init.d/apache2 restart

        #install rewrite mod
        sudo a2enmod rewrite

        sudo vim /etc/apache2/sites-available/default

        #change to

        <Directory /var/www/>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride all
            Order allow,deny
            allow from all
        </Directory>



Apache Note
^^^^^^^^^^^^^^

    tree MVC 檔案目錄結構

    ::

        test/
            app/
                controllers/
                models/
                views/
            public/
                css/
                img/
                js/
            index.php


    ::

        #test/.htaccess

        <IfModule mod_rewrite.c>
            RewriteEngine on
            RewriteRule  ^$ public/    [L]
            RewriteRule  (.*) public/$1 [L]
        </IfModule>

    ::

        #test/public/.htaccess

        <IfModule mod_rewrite.c>
            RewriteEngine On
            RewriteCond %{REQUEST_FILENAME} !-d
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^(.*)$ index.php?_url=/$1 [QSA,L]
        </IfModule>

    You also can move these configurations to the apache's main configuration file:

    ::

        <IfModule mod_rewrite.c>

            <Directory "/var/www/test">
                RewriteEngine on
                RewriteRule  ^$ public/    [L]
                RewriteRule  (.*) public/$1 [L]
            </Directory>

            <Directory "/var/www/test/public">
                RewriteEngine On
                RewriteCond %{REQUEST_FILENAME} !-d
                RewriteCond %{REQUEST_FILENAME} !-f
                RewriteRule ^(.*)$ index.php?_url=/$1 [QSA,L]
            </Directory>

        </IfModule>

Check Install
^^^^^^^^^^^^^^

    This file can implement initialization of components as well as application behavior

    ::

        <?php

            try {

                //Register an autoloader
                $loader = new \Phalcon\Loader();
                $loader->registerDirs(array(
                    '../app/controllers/',
                    '../app/models/'))->register();

                //Create a DI
                $di = new Phalcon\DI\FactoryDefault();

                //Setup the view component
                $di->set('view', function()
                    {
                        $view = new \Phalcon\Mvc\View();
                        $view->setViewsDir('../app/views/');
                        return $view;
                    }
                );



                //Setup a base URI so that all generated URIs include the "tutorial" folder
                $di->set('url', function(){
                    $url = new \Phalcon\Mvc\Url();
                    $url->setBaseUri('/tutorial/');
                    return $url;
                });

                //Handle the request
                $application = new \Phalcon\Mvc\Application($di);
                echo $application->handle()->getContent();

            } catch(\Phalcon\Exception $e) {
                echo "PhalconException: ", $e->getMessage();
            }

Createing a Controller
^^^^^^^^^^^^^^^^^^^^^^^^^

    IndexController.php

    ::

        <?php

        class IndexController extends \Pphalcon\Mvc\Controller{

            public function indexAction(){
                echo "<h1>Hello!</h1>"
            }
        }


    views app/views/index/index.phtml

    ..code-block :: php

        <?php

            echo '<h1>Hello</h1>';


    Add link app/views/index/index.phtml

    ..code-block:: php

        <?php

            echo '<h1>Hello</h1>';

            echo Phalcon\Tag::linkTo("signip", "Sign Up Here!");


    Add signup controller app/controller/SignupController.php

    ::

        <?php

            class SignupController extends \Phalcon\Mvc\Controller{

                public function indexAction(){

                }
            }

    Add sign view app/views/sign/index.phtml

    ::

        <?php use Phalcon\Tag; ?>

        <h2>Sign up using this form</h2>

        <?php echo Tag::form("signup/register"); ?>

        <p>
            <label for="name">Name</label>
            <?php echo Tag::textField("name"); ?>
        </p>

        <p>
            <label for="email">E-mail</label>
            <?php echo Tag::textField("email"); ?>
        </p>

        <p>
            <?php echo Tag::submitButton("Register"); ?>
        </p>

        </form>

    modify sign controller app/controller/SignController.php

    ..code-block :: php

        <?php

        class SignupController extends \Phalcon\Mvc\Controller{

            public function indexAction(){

            }

            public function registerAction(){

            }

        }

Creating a Model
^^^^^^^^^^^^^^^^^^

    Create DataBase and Table

    ::

        $> mysql -u root -p
        #enter password

        CREATE DATABASE users;
        USE users;
        CREATE TABLE `users` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `name` varchar(70) NOT NULL,
            `email` varchar(70) NOT NULL,
            PRIMARY KEY (`id`)
        );

    model : app/models/Users.php

    ..code-block :: php

        <?php

        class Users extends \Phalcon\Mvc\Model{

        }

    setting a Database Connection : public/index.php

    ::

        //Setup the database service
        $di->set('db', function(){
            return new \Phalcon\Db\Adpter\Pdo\Mysql(array(
                "host" => "localhost",
                "username"=>"root",
                "password"=>"root",
                "dbname"=>"users"));
        });


