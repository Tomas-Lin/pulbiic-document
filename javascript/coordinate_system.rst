Coordinate System
---------------------

Javascript 攥寫座標系統紀錄
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    座標系統 topic

    .. note ::

        定義座標系統的X軸與Y軸

        新增的tag會產生在座標系統上定位

        取得標籤的X, Y 座標

    .. code-block :: html

        <!DOCTYPE html>
       <html lang="en">
       <head>
           <meta charset="UTF-8">
           <title>Coordinate System</title>
       </head>
        <body>
            <div id='coordinate'  style='width:800px;height:600px;border:1px solid #CCC;'>
        <div id='test' style='position:relative;'>test</div>
        </div>
                <input type="number" id="x">
                <input type="number" id="y">
                <button id="move">移動</button>
        </body>

        <script src="jquery.min.js"></script>

        <script>
            $(function(){

                function Coordinate(id, LR_offset, TB_offset){
                    this.convas = $(id);
                    this.width = this.convas.width();
                    this.height = this.convas.height();
                    this.LR_offset = this.width / LR_offset;
                    this.TB_offset = this.height / TB_offset;
                }

                Coordinate.prototype.get_X = function(x){
                    return this.LR_offset * x;
                }

                Coordinate.prototype.get_Y = function(y){
                    return this.TB_offset * y;
                }


                var coordinate_box = new Coordinate('#coordinate', 8, 6);
                $("#move").bind("click", function(){
                        var x = coordinate_box.get_X($("#x").val()),
                            y = coordinate_box.get_Y($("#y").val());
                        $('#test').css('left', x+'px');
                        $('#test').css('top', y+'px');
                });

            });
        </script>

    </html>


    .. note ::

        鍵盤上下左右evnt

        標籤再座標中進行上下左右移動


Time Sync Server
-----------------

    時間同步系統 topic

    ..note ::

        抓取 NTP 時間

        倒數計時 發送 request


