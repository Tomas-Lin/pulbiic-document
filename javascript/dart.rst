Dart
--------

    v8 引擎目前負責大部分的Chrome的速度，但是有許多V8 引擎的工程師目前正在研究Dart

    是物件導向的程式語言

    Dart 可以在瀏覽器中執行，也可以獨立出來運行，你可以直接使用編輯器編寫Dart，並且啟動與調校Dart應用程序

Quckly Start
""""""""""""""

    Download and Install the software

    ::
        #64 bit

        $ wget http://storage.googleapis.com/dart-archive/channels/stable/release/latest/editor/darteditor-linux-x64.zip

        #32 bit

        $ wget http://storage.googleapis.com/dart-archive/channels/stable/release/latest/editor/darteditor-linux-ia32.zip

    unzip

    ::

        $ unzip darteditor-linux-x64.zip

    get the sample code

    ::

        $ wget https://github.com/dart-lang/one-hour-codelab/archive/master.zip

    .. note ::

        Dart 初次使用時可能會有Bug，因為libc6的套件版本問題，libc6的版本需要在2.15以上，但是debain的版本是2.13
        所以需要自己去安裝2.15以上的libc6。

    ::

        ##確認libc6的版本，若版本為2.15以上的話則不需要以下步驟
        $ sudo aptitude show libc6

        $ sudo echo 'deb http://ftp.debian.org/debian sid main' >> /etc/apt/sources.list

        $ sudo apt-get update

        $ sudo apt-get -t sid install libc6 libc6-dev libc6-dbg
        ##將deb http://ftp.debian.org/debian sid main 註解##
        $ sudo vim /etc/apt/sources.list

        $sudo apt-get update


