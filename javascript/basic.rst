Javascript
---------------------------------------------------------------

    HTTP access control

    參考資料：https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest?redirectlocale=en-US&redirectslug=DOM%2FXMLHttpRequest

    .. code-block : javascript

        var invocation = new XMLHttpRequest();
        var url = 'http://bar.other/resources/public-data/';

        function callOtherDomain() {
          if(invocation) {
            invocation.open('GET', url, true);
            invocation.onreadystatechange = handler;
            invocation.send();
          }
        }


