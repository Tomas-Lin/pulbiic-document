frozen-flask
---------------------------------------------------

    依照設定可以產生相對應的static file 儲存於build資料夾中！
    
    demo
    
    .. code-block :: python 
    
        from flask import Flask, render_template
        from flask_frozen import Freezer

        app = Flask(__name__)
                
        freezer = Freezer(app)

        @app.route('/', methods=['GET', 'POST'])
        def products_list():
            return render_template('index.html')

        @app.route('/product_<int:product_id>/', methods=['GET', 'POST'])
        def product_details(product_id):
            return render_template('product.html',product_id = product_id)


        @freezer.register_generator
        def product_details():  # endpoint defaults to the function name
            # `values` dicts
            yield {'product_id': '1'}
            yield {'product_id': '2'}
            yield {'product_id': '3'}

        @freezer.register_generator
        def product_url_generator():  # Some other function name
            # `(endpoint, values)` tuples
            yield 'product_details', {'product_id': '1'}
            yield 'product_details', {'product_id': '2'}

        @freezer.register_generator
        def product_url_generator():
            yield '/Tomas_1/'
            yield '/Tomas_2/'

        @freezer.register_generator
        def product_url_generator():
            # Return a list. (Any iterable type will do.)
            return [
                '/product_1/',
                # Mixing forms works too.
                ('product_details', {'product_id': '2'}),
            ]
    
        if __name__ =='__main__':
            freezer.run(debug=True)

Configure
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    
參考資料
--------------------------------------------------

    frozen-flask : http://pythonhosted.org/Frozen-Flask/
