Flask Basic info
---------------------------------------------------------

    Resquest 取得多個值(list)
    
        request.form.getlist('name')
        
    flask.abort
        
        Raises an HTTPException for the given status code. 
        For example to abort request handling with a page not found exception, 
        you would call abort(404).
    
    Demo
    
    .. code-block :: python
        
        from werkzeug.exceptions import HTTPException
        from flask import Flask, abort

        class PaymentRequired(HTTPException):
            code = 402
            description = '<p>You will pay for this!</p>'

        abort.mapping[402] = PaymentRequired

        app = Flask(__name__)
        @app.route('/')
        def mainpage():
            abort(402)

        @app.errorhandler(402)
        def payme(e):
            return 'Pay me!'

        app.run()
    
    
