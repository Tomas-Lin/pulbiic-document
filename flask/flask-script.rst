Flask-script
--------------------------------

    script 可以協助設定你的資料庫或處理固定時程的工作，

    DEMO

    myapp.py

    .. code-block :: python

        from flask import Flask
        app = Flask(__name__)

        @app.route("/")
        def hello():
            return "Hello World!"

        if __name__ == "__main__":
            app.run()

    script-demo.py

    .. code-block :: python

        from flask.ext.script import Manager
        from myapp import app

        manager = Manager(app)

        @manager.command
        def hello():
            print "hello"
        @manager.command
        def hello2():
            print "hello2"

        if __name__=='__main__':
            manager.run()

    commond 執行時

    ::

        $ python script-demo.py hello #執行函數名稱

    option

    script-demo.py

    .. code-block :: python

        from flask.ext.script import Manager
        from myapp import app

        manager = Manager(app)

        @manager.option('-n', '--name', help='Your name')
        def hello(name):
            print "hello", name

        if __name__=='__main__':
            manager.run()

    執行

    DEMO

    ::

        $ python script-demo.py hello -n 'Tomas'    # hello Tomas
        $ python manage.py hello --name=Joe #hello Joe


    script-demo.py

    .. code-block :: python

        from flask.ext.script import Server, Manager
        from myapp import create_app

        manager = Manager(create_app)
        manager.add_command("runserver", Server(host="0.0.0.0", port=9000))

        if __name__ == "__main__":
            manager.run()

    執行

    ::

        $ python script-demo.py runserver
