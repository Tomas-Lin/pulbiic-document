Flask-Admin
--------------------------------------
Introduction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	Flask-Admin利用簡單的class和method建立一個好用的後台模組。
	
	::
	
		class MyView(BaseView):
		@expose('/')
		def index(self):
			return self.render('admin/myindex.html')

		@expose('/test/')
		def test(self):
			return self.render('admin/test.html')
        
Quick start
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    要顯示的HTML儲存在templates/admin/內，Flask-Admin最重要的是model class以及
    和Database的溝通，可以依據個人需求對所需要的class做繼承的動作，而要擴展功能只要增加
    view就可以了。
    
Initialization
""""""""""""""""""""""""""""""""""""""
    
    ::
        
        from flask import Flask
        from flask.ext.admin import Admin

        app = Flask(__name__)

        admin = Admin(app)
        # Add administrative views here

        app.run()

    要連結到Flask-Admin的頁面網址為：http://localhost:5000/admin

    在上方會有一個Top-bar，你可以在Admin函數中設定左上角的顯示名稱。
    
    ::
    
        admin = Admin(app, name='Tomas')

    flask-admin不一定要用constructor產生物件，可以利用admin.init_app(app)進行
    應用程式的設定即可。

Adding views
""""""""""""""""""""""""""""""""""""""
        Flask-Admin可以利用class BaseView增加管理者頁面：
        
        ::
        
            from flask import Flask
            from flask.ext.admin import Admin, BaseView, expose

            class MyView(BaseView):
                @expose('/')
                def index(self):
                    return self.render('index.html')

            app = Flask(__name__)

            admin = Admin(app, name='Tomas')
            admin.add_view(MyView(name='Hello'))

            app.run()

        執行這個sample 你的Top-bar會多一個Hello的選項，每一個view都要有對應的html
        否則會無法作正確的顯示。
        
        index.html
        
        ::
        
            {% extends 'admin/master.html' %}
            {% block body %}
                Hello World from MyView!
            {% endblock %}
        
        master.html是Flask-Admin中就有預設好的html，所以在templates/admin中
        不需要有這個檔案，每個view的class 都要有@expose('/')這個預設的首頁否則會
        產生錯誤。
        
        Top-bar的選項可以設定為下拉式選單，demo為：
        
        ::
        
            from flask import Flask
            from flask.ext.admin import Admin, BaseView, expose

            class MyView(BaseView):
                @expose('/')
                def index(self):
                    return self.render('index.html')
                
                @expose('/test1/')
                def test1(self):
                    return self.render('test1.html')
                    
                @expose('/test2/')
                def test2(self):
                    return self.render('test2.html')
                    
                @expose('/test3/')
                def test3(self):
                    return self.render('test3.html')
                    
            app = Flask(__name__)
            admin = Admin(app, name='Tomas')
            admin.add_view(MyView(name='Hello 1', endpoint='test1', category='Test')) #admin/test1
            admin.add_view(MyView(name='Hello 2', endpoint='test2', category='Test')) #admin/test2
            admin.add_view(MyView(name='Hello 3', endpoint='test3', category='Test')) #admin/test3
            
            app.run()

Authentication
"""""""""""""""""""""""""""""""""

    Flask-Admin並沒有任何關於帳號權限設定的功能，當你有這類的需求時可以配合Flask-login
    達到你的需求
    
    DEMO:
    
    ::
    
        class MyView(BaseView):
        def is_accessible(self):
            return login.current_user.is_authenticated()
        
    
Generating URLs
"""""""""""""""""""""""""""""""""

    跳轉頁面的時候可以利用url_for這個函式，在網址前方需要加上一個'.'，取得local的view：
    
    ::
    
        from flask import url_for

        class MyView(BaseView):
            @expose('/')
            def index(self)
                # Get URL for the test view method
                url = url_for('.test')
                return self.render('index.html', url=url)

            @expose('/test/')
            def test(self):
                return self.render('test.html')
    
    也可以跳到另一個view：
    
    ::
    
        class MyView(BaseView):
            @expose('/')
            def index(self)
                # Get URL for the test view method
                url = url_for('test2.test')
                return self.render('index.html', url=url)

File Admin
""""""""""""""""""""""""""""""""""

    可以產生一個資料夾內的檔案列表：

    ::
    
        from flask import Flask
        from flask.ext.admin.contrib.fileadmin import FileAdmin
        from flask.ext.admin import Admin

        import os.path as op

        app = Flask(__name__)
        DEBUG = True
        SECRET_KEY = 'Tomas Demo'
        app.config.from_object(__name__)
        admin = Admin(name='My App')
        path = op.join(op.dirname(__file__), 'static')
        admin.init_app(app)
        admin.add_view(FileAdmin(path, '/static/', name='static Files'))

        app.run()

    可以建立資料夾 ，創建檔案以及修改檔名，可以設定上傳檔案， 刪除檔案 或是資料夾等等權限設定，
    
    參考網站：http://flask-admin.readthedocs.org/en/latest/api/mod_contrib_fileadmin/#module-flask.ext.admin.contrib.fileadmin
    
    (有空再整理，加入TODO)
    


Working with templates
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    摘要：
    
    You can derive from template;(可以使用現有的模板)
    
    可以自己創建模板區塊
    
    可以自己決定是否切割模板
    
    可以自己建立相同名字的模板複寫原本的模板
    
    
    預設的大的模板為admin/master.html


    ============= ========================================================================
    head_meta     head 中的meta設定標籤
    title         頁面的名稱
    head_css      head 標籤中的css
    head          head標籤
    page_body     <body>標籤
    brand         Logo in the menu bar
    body          頁面會顯示的地方
    tail          Empty area below content
    ============= ========================================================================


PyMongo backend
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Flask-Admin沒有檔案結構，這部份可以設定ModuleView達成。
    
    你在使用PyMongoDB的時候必須要注意的事項：
    
    設定colum_list顯示列表
    
    設定表單的屬性
    
    要先產生正確的PyMongo 物件。
