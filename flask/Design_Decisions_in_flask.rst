Design Decisions in Flask
---------------------------

    1. The most important one is that implicit application objects require
        that there may only be one instance at the time. There are ways to
        fake multiple applications with a single application object, like
        maintaining a stack of applications, but this causes some problems
        I won’t outline here in detail.

        最重要的一點就是可以啟動一個APP object包含多個App的應用程序，但是這會有幾個問題。

    2.

專有名詞
^^^^^^^^^^^
* implicit application objects

參考文件
^^^^^^^^^^

    http://flask.pocoo.org/docs/design/
