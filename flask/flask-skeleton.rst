Flask-Skeleton
----------------------------

    flask login 以MVC架構完成的skeleton
    
    config.py
    
    ::
    
        CSRF_ENABLED = True
        SECRET_KEY = "DO IT"
        DEBUG = True
        MONGO_DBNAME = 'login'

    run.py
    
    ::
        
        from myapp import app
        if '__main__' == __name__:
            app.run()

    myapp/__init__.py
    
    ::
    
        #-*- encoding: utf-8 -*-

        from flask import Flask
        from flask.ext.pymongo import PyMongo
        from flask.ext.admin import Admin

        app = Flask(__name__)

        app.config.from_object('config')

        mongo = PyMongo(app)

        from myapp import views

        from login import init_login
        init_login()


        from admin_views import MyAdminIndexView
        admin = Admin(app, u'start/ending 管理介面',  index_view=MyAdminIndexView())

    myapp/views.py
    
    ::
    
        from myapp import app 

        from flask import render_template,flash, url_for, redirect, request

        from flask.ext.login import login_user, current_user, \
                                    logout_user, login_required,  \
                                    fresh_login_required

        from forms import LoginForm
        from models import User

        @app.route('/')
        def index():
            return render_template('index.html', user=current_user)

        @app.route('/secret')
        @fresh_login_required
        def secret():
            return render_template("secret.html")

        @app.route('/login', methods=('GET', 'POST'))
        def login_view():
            form = LoginForm()
            if form.validate_on_submit():
                user = User(form.get_user())
                login_user(user)
                return redirect(request.args.get("next") or url_for('index'))
            return render_template('form.html', form=form)    

        @app.route('/logout')
        @login_required
        def logout_view():
            logout_user()
            return redirect(url_for('index'))
            
    myapp/forms.py
    
    ::
    
         #-*- encoding: utf-8 -*-
        from myapp import  mongo

        from flask.ext.wtf import Form, ValidationError
        from wtforms import TextField, PasswordField
        from wtforms.validators import Required
                                    
        class LoginForm(Form):
            login = TextField("name", validators=[Required(message=u"需要輸入使用者ID")])
            pwd = PasswordField("password", validators=[Required(message=u"需要輸入使用者密碼")])

            def validate_login(self, field):
                user = self.get_user()

                if user is None:
                    raise ValidationError(u'使用者帳號錯誤!')

                if user['pwd'] != self.pwd.data:
                    raise ValidationError(u'使用者密碼錯誤!')
                    
            def get_user(self):
                #用login user name 去撈資料庫看有沒有這樣的user
                return mongo.db.users.find_one({'name':self.login.data})

    myapp/login.py
    
    ::
    
        from myapp import app 

        from myapp import mongo
        from models import User

        from flask.ext.login import LoginManager

        def init_login():
            login_manager = LoginManager()
            login_manager.setup_app(app)
            
            @login_manager.user_loader
            def load_user(id):
                return User(mongo.db.users.find_one({"_id":id}))
                
    myapp/models.py
    
    ::
    
        #-*- encoding: utf-8 -*-
        class User(object):
            def __init__(self, user):
                self.id = user['_id']
                self.name = user['name']
                self.pwd = user['pwd']
            
            def is_authenticated(self):
                return True

            def is_active(self):
                return True

            def is_anonymous(self):
                return False

            def get_id(self):
                return self.id

            #給flask-admin管理介面的必備項目
            def __unicode__(self):
                return self.username

    myapp/admin_views.py
    
    ::
    
        from flask.ext.login import current_user
        from flask.ext.admin import  AdminIndexView

        class MyAdminIndexView(AdminIndexView):
            def is_accessible(self):
                return current_user.is_authenticated()
