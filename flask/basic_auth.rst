HTTP Basic Auth
-------------------------------

由Armin Ronacher提交，另有其他類似modules: `HTTP Digest Auth`_ , `Sign in with SteamID`_ , `Simple OpenID with Flask`_ 。

.. _HTTP Digest Auth : http://flask.pocoo.org/snippets/31/

.. _Sign in with SteamID : http://flask.pocoo.org/snippets/42/

.. _Simple OpenID with Flask : http://flask.pocoo.org/snippets/7/

1.簡介：
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

對於簡單的application來說，HTTP Basic Auth的登入機制已經足夠使用，

Flask要實作相當容易，下列所使用的decorator功能是針對特定的用戶。


2.Sample:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**views.py**

::

    from flask import render_template, request, Response
    from functools import wraps
    from app import app
    def check_auth(username, password):
        """This function is called to check if a username /
        password combination is valid.
        """
        return username == 'admin' and password == 'secret'
    def authenticate():
        """Sends a 401 response that enables basic auth"""
        return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})
    def requires_auth(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            auth = request.authorization
            print f(*args, **kwargs)
            if not auth or not check_auth(auth.username, auth.password):
                return authenticate()
            return f(*args, **kwargs)
        return decorated
    @app.route('/')
    @app.route('/index')
    @requires_auth
    def index():
        return render_template('base.html')

**run.py**

::

    from app import app
    app.run()
    
**app/__init__.py**

::

    from flask import Flask
    app = Flask(__name__)
    from app import views
    
**base.html**

::

    <html>
      <head>
        <title>Basic Auth Demo</title>
      </head>
      <body>
        Login success!!!
        </form>
      </body>
    </html>

3.Note:
^^^^^^^^^^^^^^^^^^^^^^^^^^^

warps : http://docs.python.org/dev/library/functools.html#functools.wraps

.. note::
     
    warps可以協助你更便利的使用 partial(update_wrapper, wrapped=wrapped, assigned=assigned, updated=updated)

    定義一個decorator函式


.. note::
	dsfdfdsf
    
4.參考網站：
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

HTTP Basic Auth : http://flask.pocoo.org/snippets/8/

**未解決問題：**

*request.authorization是什麼？*

*如何登出？*

**base.html**


