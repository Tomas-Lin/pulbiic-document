uWSGI
------------------------------

1. 簡介
^^^^^^^^^^^^^^^^^^^

    WSGI_ 是(Python Web Server Gateway Interface 的縮寫)專為Python所設計的一個
    的 Gateway_ Interface，它的功能在Server與Application或是framwork溝通時的橋樑。

    而uWSGI則是一個協助使用Server 端的 WSGI 一個套件 。**deployment option??**

    uWSGI 是一種協議同時也可以是一個應用程式伺服器， 可以支援 FastCGI_ 和HTTP協議。
    與uWSGI類似的套件有 nginx_ , lighthttpd_ , cherokee_ 。

    .. _nginx : http://nginx.org/

    .. _lighthttpd : http://www.lighttpd.net/

    .. _cherokee : http://www.cherokee-project.com/

    .. _WSGI : http://en.wikipedia.org/wiki/Web_Server_Gateway_Interface

    .. _Gateway : http://en.wikipedia.org/wiki/Gateway_(telecommunications)

    .. _FastCGI : http://flask.pocoo.org/docs/deploying/fastcgi/#deploying-fastcgi

    .. note:
        
        在執行app.run()這個函式之前一定要先確定程式的進入點是否為此模組，可以利用 
        if __name__ = '__main__' 判斷，或是將其分離至其他檔案，否則啟動的都只
        會是本機端的伺服器。

2.安裝
^^^^^^^^^^^^^^^^^^^^^

    安裝 **build-essential** 與 python-dev:

    ::

        $ sudo apt-get install build-essential python-dev
        
    安裝uwsgi:

    ::

        $ sudo pip install uwsgi
        
    或是

    ::

        $ curl http://uwsgi.it/install | bash -s default /tmp/uwsgi
        
    此指令會將uwsgi安裝在/tmp/uwsgi 裡

    或是

    ::

        $ wget http://projects.unbit.it/downloads/uwsgi-latest.tar.gz
        $ tar zxvf uwsgi-latest.tar.gz
        $ cd <dir>
        $ make
        
3.DEMO
^^^^^^^^^^^^^^^^^^^^^

    test.py:

    ::

        def application(env, start_response):
            start_response('200 OK', [('Content-Type','text/html')])
            return "Hello World"
        
    這個檔案只有一個函式，因為uWSGI python loader預設搜尋的函式名稱就是 **application**

\ **Start**\ 

    ::

        $ uwsgi --http :9090 --wsgi-file test.py
        
    --processes and --threads option:

    ::

        $ uwsgi --http :9090 --wsgi-file test.py --processes 4 --threads 2 --stats 127.0.0.1:9191
        
    上述指令可以同時開啟四個processes，可以在終端機輸入ps -ax檢查，並起在本機的9191 port
    可以看到關於此四個processes的作業狀態。 

    **threads不知道怎麼測試！囧！**

\ **configuration:**\


    uwsgi的config的副檔名是.ini:test_conf.ini

    ::

        [uwsgi]
        socket = /tmp/uwsgi
        http = :9090
        chdir = /home/horsekit/Dropbox/www/test/
        pythonpath = test.py
        module = main
        master = true
        processes = 4
        threads = 2
        stats = 127.0.0.1:9191
            
    **這個還有bug**

    設定檔存放位置在：  /etc/uwsgi/apps-available/

4.參考資料：
^^^^^^^^^^^^^^^^^^^^^

    flask ： http://flask.pocoo.org/docs/deploying/uwsgi/

    快速安裝uwsgi 與 DEMO : http://uwsgi-docs.readthedocs.org/en/latest/WSGIquickstart.html

    uwsgi config : https://uwsgi-docs.readthedocs.org/en/latest/Configuration.html?highlight=config
