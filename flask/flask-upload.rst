Flask-Upload
-----------------------------------------

    HTML form 表單中檔案上傳時要新增屬性 enctype=multipart/form-data
    
    demo:
    
    myapp.py
    
    ::
    
        import os
        from flask import Flask, request, redirect, url_for, render_template,\
            send_from_directory
        from werkzeug import secure_filename, SharedDataMiddleware


        UPLOAD_FOLDER = '/data'
        ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

        myapp = Flask(__name__)
        DEBUG = True


        myapp.config.from_object(__name__)

        myapp.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
        myapp.add_url_rule('/uploads/<filename>', 'uploaded_file',
                         build_only=True)
        myapp.wsgi_app = SharedDataMiddleware(myapp.wsgi_app, {
            '/uploads':  myapp.config['UPLOAD_FOLDER']
        })

        def allowed_file(filename):
            return '.' in filename and \
                   filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

        @myapp.route('/', methods=['GET', 'POST'])
        def upload_file():
            if request.method == 'POST':
                file = request.files['file']
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(myapp.config['UPLOAD_FOLDER'], filename))
                    return redirect(url_for('uploaded_file',
                                            filename=filename))
            return render_template('index.html')

        @myapp.route('/uploads/<filename>')
        def uploaded_file(filename):
            return send_from_directory(app.config['UPLOAD_FOLDER'],
                                       filename)
        myapp.run()


    templates/index.html
    
    ::
    
        <!doctype html>
        <html>
            <head>
                <title>Flask upload Demo</title>
            </head>
            <body>
                <form action="" method='post' enctype='multipart/form-data'>
                    <p><input type=file name=file>
                    <input type=submit value=Upload>
                </form>
            </body>
        </html>
