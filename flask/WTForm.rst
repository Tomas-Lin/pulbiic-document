Form
===================================================================================================

Basic Field 使用範例說明：
--------------------------------------------------------------------------

    +------------------------+-----------------------------------------+
    | BooleanField           | reuturn a tag <input type="checkbox">   |
    +========================+=========================================+
    | DateField              | default：%Y-%m-%d                       |
    +------------------------+-----------------------------------------+
    | DateTimeField          | default：%Y-%m-%d %H:%M:%S              |
    +------------------------+-----------------------------------------+

    +----------+-------------------------------------------------------------------------------+
    |          |File upload tag                                                                |
    |          |if any is sent by the browser in the post params                               |
    |FileField |This field will NOT actually handle the file upload portion                    |
    |          |as wtforms does not deal with individual frameworks’ file handling capabilities|
    +----------+-------------------------------------------------------------------------------+

    .. code-block:: python

        class UploadForm(Form):
            image        = FileField(u'Image File', [validators.regexp(u'^[^/\\]\.jpg$')])
            description  = TextAreaField(u'Image Description')

            def validate_image(form, field):
                if field.data:
                    field.data = re.sub(r'[^a-z0-9_.-]', '_', field.data)

        def upload(request):
            form = UploadForm(request.POST)
            if form.image.data:
                image_data = request.FILES[form.image.name].read()
                open(os.path.join(UPLOAD_PATH, form.image.data), 'w').write(image_data)

Validators
-----------------------------------------------------------------------------
    ::

        class wtforms.validators.ValidationError(message=u'', *args, **kwargs):

    WTFofrm 送出的時候會先進行驗證與檢查，可以新增一些檢查的method在class底下，若是不通過驗會產生ValidationError。

    class wtforms.validators.EqualTo(fieldname, message=None)：

        解決兩個欄位的值必須要相同的時候的驗證，常利用於申請帳號時確認密碼使用。

    class wtforms.validators.Length(min=-1, max=-1, message=None)：

        檢查Max 與 Min 長度，而message則是Error message

    class wtforms.validators.NumberRange(min=None, max=None, message=None):

        檢查數字的區段，若不在Min與Max的區段之內則回傳Error message。

Custom validators
---------------------------------------------------------------------------------

    .. code-block:: python

        class MyForm(Form):
        name = TextField('Name', [Required()])

        def validate_name(form, field):
            if len(field.data) > 50:
                raise ValidationError('Name must be less than 50 characters')

    name 送出的時候會檢查字串是否大於50個字元，若超過50個字元則停止送出表單並且產生Error message

    .. code-block:: python

        def my_length_check(form, field):
            if len(field.data) > 50:
                raise ValidationError('Field must be less than 50 characters')

        class MyForm(Form):
            name = TextField('Name', [Required(), my_length_check])

    name 送出的時候執行my_length_check method檢查字串是否大於50個字元，若超過50個字元則停止送出表單並且產生Error message

Removing Fields per-instance
------------------------------

    可以在某些特定的狀態下，使用del刪除表單內的欄位。

    .. code-block :: python

        #-*- coding:utf-8 -*-
        from flask import Flask, render_template, request
        from forms import CustomValidatorForm

        app = Flask(__name__)

        DEBUG = True
        CSRF_ENABLED = True
        SECRET_KEY = 'you-will-never-guess'
        RECAPTCHA_PUBLIC_KEY='6LflTuASAAAAAOvtAurqYAu-H6tv-RB_9XufQTWY'
        app.config.from_object(__name__)

        @app.route('/', methods=['GET', 'POST'])
        def index():
            form = CustomValidatorForm()
            if form.validate_on_submit():
                del form.name
                return render_template('del_demo.html', form=form)
            return render_template('validator.html',
                form = form)
        if '__main__' == __name__:
            app.run()

    del_demo.html

    .. code-block :: html

        <!DOCTYPE HTML>
        <html>
            <head></head>
            <body>
                <form action='' method='POST'>
                    <fieldset>
                        {{ form.hidden_tag() }}
                        {% if form.name %}
                            name: {{ form.name }} <br />
                        {% endif %}
                        <ul>
                            {% for error in form.name.errors %}
                                {% if error %}
                                    <li>
                                        <p>{{ error }}</p>
                                    </li>
                                {% endif %}
                            {% endfor %}
                        </ul>
                        password : {{ form.pwd }}
                        <ul>
                            {% for error in form.pwd.errors %}
                                {% if error %}
                                    <li>
                                        <p>{{ error }}</p>
                                    </li>
                                {% endif %}
                            {% endfor %}

                        </ul>
                        multicheckbox : {{ form.checkbox }}
                        <input type='submit' value='送出'>
                    </fieldset>
                </form>
            </body>
        </html>


Field
----------

    * errors

        type : tuple

    * process_errors

        type : tuple

    * raw_data

        type : None

    * validators

        type : tuple

    * widget

        type : None

    * _formfield

        type : true

    * _translations

        value : DummyTranslations()

widget
----------

    預設的widget

    ::
        class wtforms.widgets.ListWidget(html_tag='ul', prefix_label=True)


