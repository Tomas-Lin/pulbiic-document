Werkzeug
----------------------------------------------
    
    Werkzeug是一個實用的python 函式庫。包含了debugger
    
    demo.py:
    
    ::
    
        from werkzeug.wrappers import Request, Response
        from werkzeug.serving import run_simple

        @Request.application
        def application(request):
            return Response('Hello World!')
            
        if __name__ == '__main__':
            run_simple('localhost', 4000, application)


INSTALL
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    ::
    
        pip install Werkzeug  
        
virtualenv
""""""""""""""""""""""""""""""""""""""""""""""""

            ::
            
                pip install virtualenv
                
            因為使用Werkzeug開發的web application會有很多個，可以利用
            virtualenv 對個別的web設定個別的執行python環境。
            
Jinja2 template and Database redis
""""""""""""""""""""""""""""""""""""""""""""""""

    ::
    
        pip install jinja2
        
        sudo apt-get install redis-server

    
