Flaskr Unittest
----------------

    flask-pymongo unittest DEMO

    .. code-block :: python


    Test Skeleten Demo

    .. code-block :: python

        #-*- encoding:utf-8 -*-
        """
        exam online 測試
        """
        import os
        import unittest
        import tempfile
        from pymongo import MongoClient
        from app import app
        import json

        class FlaskTestCase(unittest.TestCase):

            def setUp(self):
                self.cls = app.test_client()

            def tearDown(self):
                self.conn.close()

            def test_conn_db(self):
                try:
                    self.conn = MongoClient()
                except:
                    raise "MongoDB Connection Error!"

            def test_write_exam(self):
                self.db = self.conn['test_exam']
                self.papers = self.db['papers']
                json_file = open('examtest.json', 'r')
                DB_data = json.loads(json_file.read())
                try:
                    self.papers.insert(DB_data)
                except:
                    raise "MongoDB Insert Error!"

            def test_exam_paper(self):
                pass

        if __name__ == "__main__":
            unittest.main()




參考網址
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

官方連結：http://flask.pocoo.org/docs/testing/
