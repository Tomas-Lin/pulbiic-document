flask-openid
----------------------

1. 安裝flask-openid
^^^^^^^^^^^^^^^^^^^^

::

     # easy_install Flask-OpenID 

或是

::

    # pip install Flask-OpenID 

2. 使用方法
^^^^^^^^^^^^^^^^^^^^

::

    from flaskext.openid import OpenID 
    oid = OpenID(app, '/path/to/store') 

OpenID預設是以檔案類型儲存認證資料，連線認證資料儲存路徑設定於/path/to/store，也可以選擇儲存於資料庫內，
路徑也可以使用OPENID_FS_STORE_PATH變數進行設定。
登入的使用者可以利用session記錄儲存，key為'openid'。
範例如下:   

::

    from flask import g, session
    
    @app.before_request
    def lookup_current_user():
        g.user = None
        if 'openid' in session:
            g.user = User.query.filter_by(openid=openid).first()


User這個範例是使用SQLAlchemy，只要上一章的Database修改為Pymongo就可以使用!
Loginhandler():
是在flask-login中的function，協助處理登入時的基本狀況:

1. 是否有儲存登入資訊，若是有則會利用get_next_url()可以直接得到帳號資料。

2. 當使用者嘗試登入，送出帳號密碼資料後，會收到回傳的openid或是錯誤訊息。

3. 認證失敗會回傳失敗訊息，可以使用fetch_error() 來得到錯誤訊息!

Login template:

::

    {% extends "layout.html" %}
    {% block title %}Sign in{% endblock %}
    {% block body %}
    <h2>Sign in</h2>
    <form action="" method=post>
        {% if error %}<p class=error><strong>Error:</strong> {{ error }}</p>{% endif %}
        <p>
        OpenID:
        <input type=text name=openid size=30>
        <input type=submit value="Sign in">
        <input type=hidden name=next value="{{ next }}">
    </form>
    {% endblock %}

after_login():

::

    from flask import flash
    @oid.after_login
    def create_or_login(resp):
        session['openid'] = resp.identity_url
        user = User.query.filter_by(openid=resp.identity_url).first()
        if user is not None:
            flash(u'Successfully signed in')
            g.user = user
            return redirect(oid.get_next_url())
        return redirect(url_for('create_profile', next=oid.get_next_url(),
                                name=resp.fullname or resp.nickname,
                                email=resp.email))


若是登入成功，帳號資訊會儲存在resp裡面，若是曾經將OpenID儲存在資料庫中，可以去資料庫撈取OpenID，若是失敗則導向回使用者頁面。

3. 顯示你的帳號資訊:
^^^^^^^^^^^^^^^^^^^^

::

    @app.route('/create-profile', methods=['GET', 'POST'])
    def create_profile():
        if g.user is not None or 'openid' not in session:
            return redirect(url_for('index'))
        if request.method == 'POST':
            name = request.form['name']
            email = request.form['email']
            if not name:
                flash(u'Error: you have to provide a name')
            elif '@' not in email:
                flash(u'Error: you have to enter a valid email address')
            else:
                flash(u'Profile successfully created')
                db_session.add(User(name, email, session['openid']))
                db_session.commit()
                return redirect(oid.get_next_url())
        return render_template('create_profile.html', next_url=oid.get_next_url())


create_profile.html

::

    {% extends "layout.html" %}
    {% block title %}Create Profile{% endblock %}
    {% block body %}
    <h2>Create Profile</h2>
    <p>
        Hey!  This is the first time you signed in on this website.  In
        order to proceed we need a couple of more information from you:
    <form action="" method=post>
        <dl>
        <dt>Name:
        <dd><input type=text name=name size=30 value="{{ request.values.name }}">
        <dt>E-Mail:
        <dd><input type=text name=email size=30 value="{{ request.values.email }}">
        </dl>
        <p>
        <input type=submit value="Create profile">
        <input type=hidden name=next value="{{ next }}">
    </form>
    <p>
        If you don't want to proceed, you can <a href="{{ url_for('logout')
        }}">sign out</a> again.
    {% endblock %}

Logging Out:

::

    @app.route('/logout')
    def logout():
        session.pop('openid', None)
        flash(u'You were signed out')
        return redirect(oid.get_next_url())

 
4. Responsive
^^^^^^^^^^^^^^^^^^^^

===============  ==========================
aim              AIM messenger address as string 
===============  ==========================
blog             Blog 網址(string)              
country          國家名稱 
date_of_birth    生日 
email            email位址               
fullname         使用者完整名稱            
gender           性別                    
icq              Icq 帳號                
identity_url     Openid (登入資訊)        
image            使用者頭相照片
**jabber**           **jabber address as string**
language         使用者預設語言            
month_of_birth   生日月份                   
msn              MSN位址                                                   
nickname         使用者暱稱                                                
phone            電話號碼                                                             
**Postcode**     **free text that should conform to the user’s country’s**
                 **postal system**
skype            SKYPE 帳號                                                          
timezone         區域時間                                                             
website          Web的網址                                                           
yahoo            Yahoo即時通的位址                                                    
year_of_birth    生日的年份                                                           
===============  ==========================

參考資料:
http://pythonhosted.org/Flask-OpenID/
http://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-v-user-logins
