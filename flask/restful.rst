Restful
----------------------------------------------------

簡介
""""""""""""""""""""""""""""""""""""""""""""""""""""

    REST Representational state transfer的縮寫，是近期為網頁與API所設計的結構規範。
    
    REST 系統主要有六個設計原則：
    
        **Client-Server** : 必須要有明確的server與client，Server提供服務給Client使用
        
        **Stateless** ： Client 送出請求石壁需要包含所有server需要的資訊，換句話說在接收到client的每一個請求，server都不需要儲存任何資訊。
        
        **Cacheable** : The server must indicate to the client if requests can be cached or not.
        
                        Client的請求都必須處存在cache裡。
                        
        **Layered System** ： Communication between a client and a server should be standardized in such a way that allows intermediaries to respond to requests instead of the end server, *without the client having to do anything different.*
        
                            Client和Server的溝通需要標準化，處理Client的請求是中介機構而不是後端的Server，和一般的客戶端不需要作任何事情的處理方式不同。
                            
        **uniform interface** : Server與Client的溝通方式必須要為unform。
        
        **Code on demand** :  Servers can provide executable code or scripts for clients to execute in their context. This constraint is the only one that is optional.
        
        



參考資料
-----------------------------------------------------

    光頭DEMO:http://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask
    
