Flask-pymongo
--------------------------

作者:Tomas

1. 安裝flask-pymongo
^^^^^^^^^^^^^^^^^^^^^

::

    # pip install Flask-PyMongo

2. 使用flask-pymongo
^^^^^^^^^^^^^^^^^^^^^

::

    from flask import Flask
    from flask.ext.pymongo import PyMongo
    
    app = Flask(__name__)
    mongo = PyMongo(app)

PyMongo預設連線MongoDB server為logcalhost  port為27017，
預設的database name是app.name。

::

    @app.route('/')
    def home_page():
        online_users = mongo.db.users.find({'online': True})
        return render_template('index.html',
            online_users=online_users)

搜尋資料庫資料並且顯示在template上。

``Collection.find_one_or_404(*args, **kwargs):``

::

    @app.route('/user/<username>')
    def user_profile(username):
        user = mongo.db.users.find_one_or_404({'_id': username})
        return render_template('user.html',
            user=user)
            

若有找到則回傳資料，若搜尋為空則回傳錯誤訊息。

Configuration:

=========================   ============================================
MONGO_URI                   MongiDB 的URI 位址
MONGO_HOST                  Host name or  IP address 。預設為localhost
MONGO_PORT                  MongoDB所監聽的port
MONGO_AUTO_START_REQUEST    Set to False to disable PyMongo 2.2’s “auto 
                            start request” behavior (see MongoClient).
                            Default: True.
                            
MONGO_MAX_POOL_SIZE         MonogoDB的連線流量大小限制
MONGO_DBNAME                Database 的名稱
MONGO_USERNAME              使用者名稱。預設:None
MONGO_PASSWORD              MongoDB 的密碼。預設:None
MONGO_REPLICA_SET           資料庫資料是否可以被copy
MONGO_READ_PREFERENCE       Determines how read queries are routed to 
                            the replica set members. 
                            Must be one of PRIMARY, SECONDARY, or 
                            SECONDARY_ONLY, or the string names thereof. 
                            Default PRIMARY.
                            
MONGO_DOCUMENT_CLASS        PyMongo回傳的資料型態。預設:dict
=========================   ============================================

多重連接範例:

::

    app = Flask(__name__)
    
    # connect to MongoDB with the defaults  
    mongo1 = PyMongo(app)
    
    # connect to another MongoDB database on the same host
    app.config['MONGO2_DBNAME'] = 'dbname_two'
    mongo2 = PyMongo(app, config_prefix='MONGO2')
    
    # connect to another MongoDB server altogether
    app.config['MONGO3_HOST'] = 'another.host.example.com'
    app.config['MONGO3_PORT'] = 27017
    app.config['MONGO3_DBNAME'] = 'dbname_three'
    mongo3 = PyMongo(app, config_prefix='MONGO3')
    
3. 參考資料:
https://flask-pymongo.readthedocs.org/en/latest/
