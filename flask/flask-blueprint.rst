flask-blueprint
----------------------------------------------

    每一組blueprint，都可以視為一張藍圖，blueprint 中有view 的集合，
    
    每一個URL對應一個View Class，或是一個View Function，而一個Application則是
    
    一個blueprint的集合。

	blueprint 提供模板，靜態文件...等等工具，blueprint 本身3並不是應用程式也沒有
	
	View的功能。
	
    DEMO

    blueprintdemo.py

    .. code-block  :: python

        from flask import Blueprint, render_template, abort
        from jinja2 import TemplateNotFound

        simple_page = Blueprint("simple page", __name__, template_folder='templates')

        @simple_page.route('/', defaults={"page": "index"})

        @simple_page.route('/<page>')
        def show(page):
            try:
                return render_template('pages/%s.html' % page)
            except TemplateNotFound:
                abort(404)

    app.py

    ..code-block :: python

        from flask import Flask
        from blueprintdemo import simple_page

        app = Flask(__name__)
        app.register_blueprint(simple_page)

        app.run()

Static File
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    blueprint 可以指定static 資料夾，也可以選擇資料夾裡面的檔案利用url_for指定

    DEMO

    app.py

    ..code-block :: python

        from flask import Flask
        from blueprintdemo import simple_page

        app = Flask(__name__)

        admin = Blueprint('admin', __name__, static_folder='static')
        style = url_for('admin.static', filename='style.css')

        app.register_blueprint(simple_page)

        app.run()
