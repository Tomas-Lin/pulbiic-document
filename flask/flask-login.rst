Flask-Login
------------------------------

簡介：
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    \ **Flask-login**\ 在Flask中負責登入，登出機制控管以及某個允許的時間區段內在 session_ 中儲存使用者的相關資訊的功能。

    Flask-login 可以

        在session中儲存使用者的ID，當使用者在登入或是登出的時候更為便利。

        設定使用者可以使用的view。

        簡單的完成 rememberme 的動作

        避免hack使用cookie登入，保護網站安全。

        整合 Flask-principal 或是其他驗證機制擴展功能。

        不限制驗證方法登入，你可以配合自用的登入機制或是OpenID或是其他方式登入。

    Flask-login 無法做到的事情

        並不強制綁定一個資料庫，或是加入一個儲存的函式。\ **Flask-login**\只是單純的取得使用者資訊。

        只處理登入登出之外沒有其他處理權限的功能。

        無法處理註冊或是恢復帳號等功能。

安裝flask-login:
""""""""""""""""""""""""""""""

    ::

        $ pip install flask-login


初始化設定你的Application:
""""""""""""""""""""""""""""""

    ::

        login_manager = LoginManager()
        login_manager.init_app(app)

        #init_app在協助你設定你的application中的before_request以其after_request並且利用loginmanager的class產生一個app.login_manager的物件。


User class:
""""""""""""""""""""""""""""""

    ::

        class User(UserMixin):
            def __init__(self, name, id, active=True):
                self.name = name
                self.id = id
                self.active = active

            def is_active(self):
                return self.active

    .. note ::

        User class是協助處理帳號權限，他可以辨別訪客，會員的停權與驗證，以及取得會員的ID，而UserMixin則是flask-login已經寫好的基本User class，可以
        透過繼承它來簡化自己的User class。

    ::

        USERS = {
            1: User(u"Notch", 1),
            2: User(u"Steve", 2),
            3: User(u"Creeper", 3, False),
        }

        USER_NAMES = dict((u.name, u) for u in USERS.itervalues()) #利用迴圈產生一個物件，手法應用

        #模擬Database

    ::

        @login_manager.user_loader
        def load_user(userid):
            return USERS.get(int(userid))

            #login_manager.user_loader 要傳入一個unickod的ID，它會檢查session中是否有user，若有則回傳一個物件若不存在則回傳None。

    ::

        login_user(USER_NAMES[username])

        #嘗試user login的動作，傳入的參數是利用User class產生的物件，會檢查method is_active的回傳值若是True則會登入成功，若是False則登入失敗。

    ::

        @login_required

        #這是一個decorate function ，放置於需要進行驗證的頁面，若你尚未登入他會執行unauthorized()，回傳一個錯誤訊息，阻擋使用者進入頁面。

    ::

        logout_user()

        #將使用者在session中的資料移除，並且若是cookie中有存使用者資料也會一併移除。


    ::

        class Anonymous(AnonymousUser):
        name = u"Anonymous"

        #訪客的class，AnonymousUser也是在flask-login已經幫你寫好了一個匿名帳號專用的class只要繼承它的method就可以簡單的作使用。



完整的DEMO:
"""""""""""""""""""""""""""""""""""""

    ::

        #-*-coding:utf-8-*-
        from app import app
        from flask.ext.login import LoginManager
        from flask import session, redirect, render_template, flash, request, url_for
        from forms import LoginForm
        from flask.ext.login import login_user,login_required, logout_user, UserMixin, AnonymousUser
        from user import User

        app = Flask(__name__)
        login_manager = LoginManager() #~產生一個Object
        login_manager.setup_app(app) #~基本配置

        SECRET_KEY = 'you-will-never-guess'

        #User class block
        #----------------------------------------------------
        class User(UserMixin):
            def __init__(self, name, id, active=True):
                self.name = name
                self.id = id
                self.active = active

            def is_active(self):
                return self.active

        #模擬USER DATABASE
        #----------------------------------------------------
        USERS = {
            1:User(u'Tomas', 1),
            2:User(u'Simon', 2),
            3:User(u'John', 3, False)
        }

        USER_NAMES = dict((u.name, u) for u in USERS.itervalues())
        #----------------------------------------------------

        @login_manager.user_loader
        def load_user(id):
            return USERS.get(int(id))  #回傳一個unicode ID 若是未登入則回傳None

        @app.route('/logout')
        @login_required #
        def logout():
            logout_user() #登出函式
            return redirect('/')
        @app.route('/')
        @app.route("/login", methods=["GET", "POST"])
        def login():
            form = LoginForm()
            if form.validate_on_submit():
                username = request.form['username']
                if username in USER_NAMES:
                    login_user(USER_NAMES[username]) #登入函式
                    return redirect(request.args.get('next') or url_for("index"))
                return redirect(url_for("login"))
            return render_template("login.html", form=form)
        @app.route('/index')
        @login_require
        def index():
            return " Login sucess! I'm index"

        @login_manager.unauthorized_handler
        def unauthorized():
            #收到錯誤後的動作
            a_response = 'login fail'
            return a_response

    登入錯誤處理：

    ::

        @login_manager.needs_refresh_handler
            def refresh():
                # do stuff
                return a_responses

    此範例登入錯誤時，request.args.get('next')會將頁面導向接下來的這一個函式，並且執行函式裡的動作。


Alternative Tokens
""""""""""""""""""""

    將資訊儲存在cookie中並不是一個安全的作法，較為安全的作法是結合帳號與密碼的驗證方式，你可以在User class中加入一個get_auth_token()，
    這個函式應該取得的是唯一識別碼，如同使用者的UID。

    同時在login_manager中也要設定一個token_loader的函式來與get_auth_token聯合運用。

    其中有另外一個function make_secure_token 可以建立一個auth_token ，這個token會連接所有的參數，並且用process的密鑰來確保資料的安全。

     (If you store the user’s token in the database permanently, then you may wish to add random data to the token to further
     impede guessing.)

    若是你的使用密碼來驗證，他舊有的auth_token失效。

Fresh Logins
""""""""""""""""""""""""""

    當使用者第一次登入時，session會紀錄狀態為fresh，但是當他登出重新登入，或是驗證毀損重新驗證後，session會紀錄該會員的狀態為'non-fresh'，
    再登入時不會有任何不同，有些時候的行為需要初次登入時進行，則可以利用這個狀態來做驗證判別。

    fresh_login_required除了驗證用戶登入也可以確保他們是初次登入，如果不是會送他們到另一個頁面。

    你也設定LoginManager.refresh_view 和 needs_refresh_message:

    login_manager.refresh_view = "accounts.reauthenticate"
    login_manager.needs_refresh_message = (u"To protect your account, please reauthenticate to access this page.")
    Or by providing your own callback to handle refreshing:

Session Protection
"""""""""""""""""""""""

    使用remember me 來記住登入資訊是不安全的，flask會協助進行安全性控管，保護你的資料外洩。

    你可以在LoginManager中設定保護模式，方法如下：

    login_manager.session_protection = "strong"

    Or, to disable it:

    login_manager.session_protection = None

    預設是basic模式。這個設定會再SESSION_PROTECTION為None, basic 或是 strong

    session 保護機制開啟的時候，每一個request都會有一個標示符號(基本上是用戶的IP和用戶代理的MD5值)，如果標示符相匹配，則該請求則允許。

3.Login session control
^^^^^^^^^^^^^^^^^^^^^^^^

login_user(user, remember=False, forcel=False)
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    .. code-block :: python

        def login_user(user, remember=False, force=False):
            '''
            Logs a user in. You should pass the actual user object to this. If the
            user's `is_active` method returns ``False``, they will not be logged in
            unless `force` is ``True``.

            This will return ``True`` if the log in attempt succeeds, and ``False`` if
            it fails (i.e. because the user is inactive).

            :param user: The user object to log in.
            :type user: object
            :param remember: Whether to remember the user after their session expires.
                Defaults to ``False``.
            :type remember: bool
            :param force: If the user is inactive, setting this to ``True`` will log
                them in regardless. Defaults to ``False``.
            :type force: bool
            '''
            if not force and not user.is_active():
                return False

            user_id = getattr(user, current_app.login_manager.id_attribute)()
            session['user_id'] = user_id
            session['_fresh'] = True
            session['_id'] = _create_identifier()

            if remember:
                session['remember'] = 'set'

            _request_ctx_stack.top.user = user
            user_logged_in.send(current_app._get_current_object(), user=_get_user())
            return True

    .. note ::

        session["user_id"] 紀錄user的id

        session["_fresh"]=True #紀錄該帳號是否為fresh

        session["_id"] = _create_identifier() (不知道是啥)

loginmanager._create_identifier()
""""""""""""""""""""""""""""""""""""

    創建一個獨有的標示符號

    .. code-block :: python

        def _create_identifier():
            user_agent = request.headers.get('User-Agent')
            if user_agent is not None:
                user_agent = user_agent.encode('utf-8')
                base = '{0}|{1}'.format(_get_remote_addr(), user_agent)
                if str is bytes:
                    base = unicode(base, 'utf-8', errors='replace')
                    h = md5()
                    h.update(base.encode('utf8'))
                    return h.hexdigest()

    .. note ::

        User_Agent ：request.headers.get('User-Agent')，取回brower中header中的User-Agent的值，
        另外也可以使用request.user_agent()。

        bytes 在python 中是一個不可變得資料型態，bytesarray是一個可變得序列。


    getattr example

    .. code-block :: python

        li=["tomas", "simon"]
        print li

        getattr(li, "append")("Moe")
        print li

        getattr(li, "pop")()
        print li

loginmanager.login_required
"""""""""""""""""""""""""""""

    python decorators method

    .. code-block :: python

        def login_required(func):
            '''
            If you decorate a view with this, it will ensure that the current user is
            logged in and authenticated before calling the actual view. (If they are
            not, it calls the :attr:`LoginManager.unauthorized` callback.) For
            example::

                @app.route('/post')
                @login_required
                def post():
                    pass

            If there are only certain times you need to require that your user is
            logged in, you can do so with::

                if not current_user.is_authenticated():
                    return current_app.login_manager.unauthorized()

            ...which is essentially the code that this function adds to your views.

            It can be convenient to globally turn off authentication when unit
            testing. To enable this, if either of the application
            configuration variables `LOGIN_DISABLED` or `TESTING` is set to
            `True`, this decorator will be ignored.

            :param func: The view function to decorate.
            :type func: function
            '''
            @wraps(func)
            def decorated_view( \*args, \*\*kwargs):
                if current_app.login_manager._login_disabled:
                    return func(\*args, \*\*kwargs)
                elif not current_user.is_authenticated():
                    return current_app.login_manager.unauthorized()
                return func(\*args, \*\*kwargs)
            return decorated_view

    .. note ::

        login_required 檢查是否登入的驗證decorators，驗證通過則顯示否則將會依照設定跳轉頁面。

        self._login_disabled = app.config.get('LOGIN_DISABLED',
                                                      app.config.get('TESTING', False))


loginmanager.fresh_login_required
"""""""""""""""""""""""""""""""""""

    驗證是否為初次登入

    .. code-block :: python

        def fresh_login_required(func):
            '''
            If you decorate a view with this, it will ensure that the current user's
            login is fresh - i.e. there session was not restored from a 'remember me'
            cookie. Sensitive operations, like changing a password or e-mail, should
            be protected with this, to impede the efforts of cookie thieves.

            If the user is not authenticated, :meth:`LoginManager.unauthorized` is
            called as normal. If they are authenticated, but their session is not
            fresh, it will call :meth:`LoginManager.needs_refresh` instead. (In that
            case, you will need to provide a :attr:`LoginManager.refresh_view`.)

            Behaves identically to the :func:`login_required` decorator with respect
            to configutation variables.

            :param func: The view function to decorate.
            :type func: function
            '''
            @wraps(func)
            def decorated_view(\*args, \*\*kwargs):
                if current_app.login_manager._login_disabled:
                    return func(\*args, \*\*kwargs)
                elif not current_user.is_authenticated():
                    return current_app.login_manager.unauthorized()
                elif not login_fresh():
                    return current_app.login_manager.needs_refresh()
                return func(\*args, \*\*kwargs)
            return decorated_view

    .. note ::

        login_fresh method 檢查session 的 _fresh 是否為 fresh。

        #: A proxy for the current user. If no user is logged in, this will be an
        #: anonymous user
        current_user = LocalProxy(lambda: _get_user() or current_app.login_manager.anonymous_user())

LoginManager.unauthorized()
""""""""""""""""""""""""""""

    user登入失敗時會自動導向到 'login_view'

    .. code-block :: python

        def unauthorized(self):
            '''
            This is called when the user is required to log in. If you register a
            callback with :meth:`LoginManager.unauthorized_handler`, then it will
            be called. Otherwise, it will take the following actions:

                - Flash :attr:`LoginManager.login_message` to the user.

                - Redirect the user to `login_view`. (The page they were attempting
                  to access will be passed in the ``next`` query string variable,
                  so you can redirect there if present instead of the homepage.)

            If :attr:`LoginManager.login_view` is not defined, then it will simply
            raise a HTTP 401 (Unauthorized) error instead.

            This should be returned from a view or before/after_request function,
            otherwise the redirect will have no effect.
            '''
            user_unauthorized.send(current_app._get_current_object())

            if self.unauthorized_callback:
                return self.unauthorized_callback()

            if not self.login_view:
                abort(401)

            if self.login_message:
                flash(self.login_message, category=self.login_message_category)

            return redirect(login_url(self.login_view, request.url))

    .. note ::

        若是'login_view'未宣告，則會送出 HTTP ERROR 401(Unauthorized)，可以導向next的變數


LoginManager.needs_refresh()
""""""""""""""""""""""""""""""

    當登入時需要身份為fresh，但該會員不是fresh時，將頁面自動導向'reefresh_view'頁面

    .. code-block :: python

        def needs_refresh(self):
            '''
            This is called when the user is logged in, but they need to be
            reauthenticated because their session is stale. If you register a
            callback with `needs_refresh_handler`, then it will be called.
            Otherwise, it will take the following actions:

                - Flash :attr:`LoginManager.needs_refresh_message` to the user.

                - Redirect the user to :attr:`LoginManager.refresh_view`. (The page
                  they were attempting to access will be passed in the ``next``
                  query string variable, so you can redirect there if present
                  instead of the homepage.)

            If :attr:`LoginManager.refresh_view` is not defined, then it will
            simply raise a HTTP 403 (Forbidden) error instead.

            This should be returned from a view or before/after_request function,
            otherwise the redirect will have no effect.
            '''
            user_needs_refresh.send(current_app._get_current_object())

            if self.needs_refresh_callback:
                return self.needs_refresh_callback()

            if not self.refresh_view:
                abort(403)

            flash(self.needs_refresh_message,
                  category=self.needs_refresh_message_category)

            return redirect(login_url(self.refresh_view, request.url))

    .. note ::

        若是'LoginManager.refresh_view'未宣告，頁面將會顯示HTTP ERROR 403



4.參考資料：
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

flask-login : https://flask-login.readthedocs.org/en/latest/

DEMO 參考：

.. 連結資訊：

.. _session : https://en.wikipedia.org/wiki/Session_(computer_science)

.. _cookie : https://en.wikipedia.org/wiki/HTTP_cookie
