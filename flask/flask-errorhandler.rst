Flask Error Handler
-------------------------------------------

    400 錯誤的回傳訊息
    
    401 會員未登入
    
    403 權限不足
    
    404 無此網頁
    
    405 不允許此method
    
    406 不接受連線
    
    408 連線時間過長
    
    409 衝突錯誤
    
    410 網頁消失
    
    411 Length Required
    
    412 前提條件失敗
    
    413 Request 資料過大
    
    414 Url 資料過大
    
    415 不支援Media 類型
    
    416 請求範圍不符合
    
    429 Request 過多
    
    500 伺服器發生錯誤
    
    502 Gateway錯誤
    
    503 服務不可使用
    
    
    
Error Handlers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        Error Handlers其實是一個view function，當error發生的時候會自動導向，
        顯示該頁面的資訊，
        
        demo:
        
        myapp.py
        
        ::
        
            from flask import Flask, render_template


            myapp = Flask(__name__)

            @myapp.route('/')
            def index():
                return 'index'
                
            @myapp.errorhandler(404)
            def page_not_found(e):
                return render_template('404.html'), 404
                
            myapp.run()

        layout.html
        
        ::
        
            <html>
            <head>
                {% block title %}{% endblock %}
            </head>
                <body>
                    {% block body %}{% endblock %}
                </body>
            </html>


        404.html
        
        ::
        
            {% extends "layout.html" %}
            {% block title %}Page Not Found{% endblock %}
            {% block body %}
              <h1>Page Not Found</h1>
              <p>What you were looking for is just not there.
              <p><a href="{{ url_for('index') }}">go somewhere nice</a>
            {% endblock %}

參考資料
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        Flask Error Handlers:http://werkzeug.pocoo.org/docs/exceptions/#werkzeug.exceptions.HTTPException
