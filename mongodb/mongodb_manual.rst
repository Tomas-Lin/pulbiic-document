MongoDB Manual
-----------------

Indexes
^^^^^^^^^^

    Index 儲存於**記憶體**，可以增加搜尋效率。

    MongoDB 提供多種儲存Indexes的方式

        * Default _id

            儲存MongoDB的預設 _id值

        * single field

            MongoDB支援user 自定義的Indexes

        * compound Index(複和索引)

            MongoDB 支援user 自定義的多重Index，類似single-field，最多支援31個欄位的複和式索引

            複合式索引不能夠使用hashed field，如果你的複合式索引內有一個是hashed
            field無法執行並且會收到一個錯誤

補充
^^^^^^^^

    hashed index :

        新增於Version 2.4，不支援multi-key field支援sharding 叫作hashed shard key，
        在sharding中使用hashed shard key*可以更均勻的分散數據*，不支援range queries
        **however, you can create both a hashed index and an ascending/descending
        (i.e. non-hashed) index on the same field:I MongoDB will use the scalar index
        for range queries**

        .. note ::

            64 bit的系統會自動將float轉為int 再做hash的動作，為了防止衝突在64bit的系統不要
            設定float 而32 bit的hash 最多到小數點後第2位

