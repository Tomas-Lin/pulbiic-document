Mongoengine
---------------------------------------------------------------------

    Getting Start

    安裝Mongoengine

    ::

        $ pip install mongoengine

    DEMO

    demo.py

    .. code-block :: python

        from mongoengine import connect
        connect("test")

    Users object

    .. code-block :: python

        class User(Document):
            email = StringField(required=True)
            first_name = StringField(max_length=50)
            last_name = StringField(max_length=50)

        ross = User(email='ross@example.com', first_name='Ross', last_name='Lawley').save()

    Insert Data

    DEMO

    insert.py

    .. code-block :: python

        from mongoengine import connect, StringField, Document, \
            ReferenceField, ListField, EmbeddedDocumentField, EmbeddedDocument

        connect("test")
        class User(Document):
            email = StringField(required=True)
            first_name = StringField(max_length=50)
            last_name = StringField(max_length=50)

        ross = User(email='ross@example.com')
        ross.first_name = 'Ross'
        ross.last_name = 'Lawley'
        ross.save()

    Embedding Insert

    DEMO

    embedding_insert.py

    .. code-block :: python

        from mongoengine import connect, StringField, Document, \
            ReferenceField, ListField, EmbeddedDocumentField, EmbeddedDocument

        connect("test")
        post1 = TextPost(title='Fun with MongoEngine', author=john)
        post1.content = 'Took a look at MongoEngine today, looks pretty cool.'
        post1.tags = ['mongodb', 'mongoengine']
        post1.save()

        post2 = LinkPost(title='MongoEngine Documentation', author=ross)
        post2.link_url = 'http://docs.mongoengine.com/'
        post2.tags = ['mongoengine']
        post2.save()

Flask Mongoengine Configure
""""""""""""""""""""""""""""

    ::

        $ pip install flask-mongoengine

    .. code-block :: python

        from flask import Flask
        from flask.ext.mongoengine import Mongoengine

        app = Flask(__name__)
        app.config["MONGODB_SETTINGS"] = {"DB":"my_tumble_log"}
        db = Mongoengine(app)

    設定設定key值(DB, USERNAME, PASSWORD, HOST, PORT)

Flask Mongoengine and WTForm
""""""""""""""""""""""""""""""

    DEMO

    .. code-block :: python

        from flask.ext.mongoengine.wtf import model_form

        class User(db.Document):
            email = db.StringField(required=True)
            first_name = db.StringField(max_length=50)
            last_name = db.StringField(max_length=50)

        class Content(db.EmbeddedDocument):
            text = db.StringField()
            lang = db.StringField(max_length=3)

        class Post(db.Document):
            title = db.StringField(max_length=120, required=True)
            author = db.ReferenceField(User)
            tags = db.ListField(db.StringField(max_length=30))
            content = db.EmbeddedDocumentField(Content)

        PostForm = model_form(Post)

        def add_post(request):
            form = PostForm(request.POST)
            if request.method == 'POST' and form.validate():
                # do something
                redirect('done')
            return render_response('add_post.html', form=form)

Support Field
""""""""""""""""

    StringField

    BinaryField

    URLField

    EmailField

    IntField

    FloatField

    DecimalField

    BooleanField

    DateTimeField

    ListField (using wtforms.fields.FieldList )

    SortedListField (duplicate ListField)

    EmbeddedDocumentField (using wtforms.fields.FormField and generating inline Form)

    ReferenceField (using wtforms.fields.SelectFieldBase with options loaded from QuerySet or Document)

    DictField


API Reference
""""""""""""""

    class mongoengine.queryset.QuerySet(document, collection)


        insert(doc_or_docs, load_bulk=True, write_concern=None)

            Parameters::

               *  docs_or_doc – a document or list of documents to be inserted(optional)

               * (load_bulk) – If True returns the list of document instances

               * write_concern – Extra keyword arguments are passed down to insert() which
                 will be used as options for the resultant getLastError command. For example,
                 insert(..., {w: 2, fsync: True}) will wait until at least two servers have
                 recorded the write and will force an fsync on each server being written to.

        update(upsert=False, multi=True, write_concern=None, full_result=False, \**update)

            Parameters::

                * upsert – 存在的document 會複寫ObjectID

                * multi – 同時修改多個document

                * write_concern – Extra keyword arguments are passed down which will
                  be used as options for the resultant getLastError command. For example,
                  save(..., write_concern={w: 2, fsync: True}, ...) will wait until at
                  least two servers have recorded the write and will force an fsync on
                  the primary server.

                * full_result – Return the full result rather than just the number updated.

                * update – Django-style update keyword arguments


