GridFS
---------

大綱
^^^^^^

    * GridFS 用來檢索或是儲存超過16MB的BSON-document

    * 什麼時候該使用 GridFS ？

        1. 如果你的檔案系統有限制目錄的檔案數量，你可以利用GridFS儲存文件

        2. 保持文件與數據跨多個系統和設備佈署

        3. 你希望可以閱讀大型文件內容而不需要完整的載入整份文件，GridFS可以協助呼叫部份的內容，
            不需要將整份文件載入記憶體

    * 不該使用GridFS

        1. 需要小部份的更新文件內容，替代翻案是可以儲存多個版本的，而指定最新的版本。

        2. 每次更新都必須更新全部內容的文件

    * 若你的文件是小於16MB的BSON，可以使用BinData數據類型來儲存二進制數據


GridFS Collections
^^^^^^^^^^^^^^^^^^^

    GridFS 儲存在兩個 collections

        * chunks 儲存binary chunks

            .. code-block :: json

                {
                    "_id" : <ObjectId>,
                    "files_id" : <ObjectID>,
                    "n" : <num>,
                    "data" : <binary>
                }

                _id : ObjectId

                files_id : 檔案的 ObjectId

                n : 序號，從0開始

                data : BSON的二進制資料

        * files 儲存檔案的metadata

            .. code-block :: json

                {
                    "_id" : <ObjectID>,
                    "length" : <num>,
                    "chunkSize" : <num>
                    "uploadDate" : <timestamp>
                    "md5" : <hash>

                    "filename" : <string>,
                    "contentType" : <string>,
                    "aliases" : <string array>,
                    "metadata" : <dataObject>,
                }

                _id : ObjectId

                length : 文件檔案大小

                chunksize : 區塊大小

                uploadDate : 初次儲存或是最後一次修改時間

                md5 : 從filemd5 API回傳的 MD5 hash 值

                filename : 檔案名稱

                contentType : MIME type

                aliases : 關鍵字陣列

                metadata : 備註

延伸閱讀
^^^^^^^^^^

    When should I use GridFS?

        http://docs.mongodb.org/manual/faq/developers/#faq-developers-when-to-use-gridfs

    Drive of BinData

        http://docs.mongodb.org/manual/applications/drivers/
