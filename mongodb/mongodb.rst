2Mongodb
=====================

.. toctree::

MongoDB教學
-----------------

翻譯者: Tomas、卓青

原文連結:
`在此 <http://www.hacksparrow.com/the-mongodb-tutorial.html>`_

使用文件之前請先安裝MongoDB。
連結:


安裝好後啟動MongoDB server

::

    $ mongod

啟動MongoDB Client 使用mongo client連線到MongoDB server:

::

    $ mongo

**注意:本文件的所有MongoDB 指令在Client 端都在 “>” 此符號後輸入。**

Getting acquainted with MongoDB
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


MongoDB 是一個
:command:`文件導向式`
的資料庫系統(一種NoSQL資料庫)，MongoDB 的結構依序是

資料庫(database)>
:command:`集合(collection)`
>文件(document)>
:command:`區域(field)`
> 索引:值(key:value)。


.. note::

    kevin's note：
    集合(collection)是指BSON( “BSON” is a portmanteau of the words “binary” and “JSON”)
    documents的分組方式，Collections不是強制性的架構，有點類似RDBMS（Relational Database Management System）的tables，
    可以有很多不同的documents在一個collection的範圍內，但在一個相同的collection內的documents通常都會有類似或有相關聯目的的應用。

    `資料來源 <http://docs.mongodb.org/manual/reference/glossary/#term-collection>`_

    區域(field)就是
    `長這個樣子 <http://docs.mongodb.org/manual/core/document/#structure>`_



下方為MongoDB 與 SQL資料模型的比較圖：

============ ============
MongoDB      SQL
------------ ------------
database     database
collection   table
document     row
field        -
{key:value}  -
============ ============


**要熟悉記住上方的對應表**, 因為接下來都是用MongoDB的用語來說明，不會再出現像是
:command:`row, table`
這些SQL資料庫會出現的術語。

要熟悉MongoDB術語~~!!

MongoDB shell是一個完整的 **javascript shell** ，你可以在在MongoDB shell **直接執行**
javascript對資料的運算或是與MongoDB指令配合使用寫入資料庫。

MongoDB shell 支援所有標準的javascript API ，這是MongoDB重要的特色之一。

MongoDB shell 是一個提供全功能的JavaScript shell , 你可以在加入或檢索查詢資料之前,執
行對資料的JavaScript運算。

MongoDB shell支援所有標準JavaScript API ,以及包含Mongo
所獨有的客製化API, 這些特色讓MongoDB成為非常強大的資料庫系統。

在你 **整理資料庫** 中的資料之前，你必須
:command:`先了解 如何分配資料庫內的資料` ，這是準備開始與
MongoDB互動的基礎能力。

在開始 **操作資料庫** 資料之前，你必須
:command:`先了解如何跟資料庫系統做溝通`， 這是你跟 MongoDB互動的基礎。

查詢在系統中的資料庫列表:

::

    > show dbs
    bands 0.203125GB
    movies (empty)
    scores (empty)

跟使用其他SQL資料庫一樣(例如MySQL), 你首先選擇要使用來執行資料運算的資料庫：

::

    > use movies
    switched to db movies

顯示目前正在使用的資料庫:

::

    > db.getName()
    movies

顯示所有的資料庫

::

    > show dbs

刪除(delete / drop )你正在使用的資料庫: ( **刪除前需確定你選擇的資料庫是否正確。** )

::

    > use scores
    > db.dropDatabase()
    {"dropped" : "scores", "ok" : 1 }

顯示所選擇的資料庫的所有集合(collections)，下列兩種方法皆可:

::

    > show collections

或是

::

    > db.getCollectionNames()

不像 `SQL <https://zh.wikipedia.org/wiki/SQL>`_ 型態的資料庫,
MongoDB建立database是很被動的, 並不需要使用者明確的下命
令來建立資料庫 ( 例如MySQL資料庫要下create database指令才會建立資料庫, 但是
MongoDB 不用 )。
MongoDB會依照情況確定要建立資料庫的時候才會自動建立。

MongoDB建立資料庫是很方便的，你不需要輸入建立資料庫的指令，當你寫入第一筆資料
的時候，MongoDB會自動幫你建立資料庫。

要建立一個新資料庫, 首先要使用use命令切換到新的資料庫, 然後執行儲存一些資料到資料
庫中, 這個時候MongoDB就會幫你建立資料庫了。

對movies寫入第一筆資料，MongoDB自動建立movies資料庫並且將資料寫入資料庫內。

我們建立一個新的資料庫叫作 movies:

::

    > use movies
    switched to db movies
    > db.comedy.insert({name:"Bill & Ted's Excellent Adventure",
    year:1989})

:command:`建立collection跟建立資料庫的方式一樣，寫入第一筆資料後會自動建立一個collection，
寫入資料指令中的comedy是collection名稱。`

上面命令會產生建立一個叫作movies的資料庫, 在這過程中, 一個叫作comedy的collection
被建立到資料庫中。

Getting your hands dirty with data in MongoDB
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

這邊用範例來說明MongoDB 如何CRUD(建立、讀取、修改、刪除)資料。

注意所有MongoDB指令都是使用MongoDB資料庫物件db的method, db所參照的資料庫
就是你使用use指令所選擇的資料庫。

Create / add data in MongoDB
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

在collection裏面建立一些documents(文件):

::

    > db.comedy.insert({name:"Wayne's World", year:1992})

::

    > db.comedy.insert({name:'The School of Rock', year:2003})

上述兩個指令對名稱為comedy的collection寫入兩筆資料。

save()與insert()結果都是一樣寫入資料，使用方法也一致。

::

    > db.comedy.save({name:"Wayne's World", year:1992})
    > db.comedy.save({name:'The School of Rock', year:2003})

insert和save的差別在於:

insert新增一筆新的docuement;

save是如果這個沒有符合這個物件的_id key值就新增, 若有_id key存在則update(更新)在資
料庫符合_id鍵值的資料。

**如果非必要最好是使用insert()新增資料，update()修改資料。**
**建議使用insert新增documents, 用update命令更新在collection裏面的docuement資料。**

Read data from MongoDB
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

從collection裏面讀資料: (讀取comdy裡的資料)

::

    > db.comedy.find()
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill &
    Ted's Excellent Adventure", "year" : 1989 }
    { "_id" : ObjectId("4e9ebb478c02b838880ef413"), "name" : "Wayne's
    World", "year" : 1992 }
    { "_id" : ObjectId("4e9ebd5d8c02b838880ef414"), "name" : "The
    School of Rock", "year" : 2003 }

讀取collection資料後用limit()限定顯示資料筆數2筆:

::

    > db.comedy.find().limit(2)
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill &
    Ted's Excellent Adventure", "year" : 1989 }
    { "_id" : ObjectId("4e9ebb478c02b838880ef413"), "name" : "Wayne's
    World", "year" : 1992 }

跟find().limit(1)相似的指令是findOne(), 只會傳回collection中的一筆document。

::

    db.comedy.findOne()
    {
    "_id" : ObjectId("4e9ebb318c02b838880ef412"),
    "name" : "Bill & Ted's Excellent Adventure",
    "year" : 1989
    }

附有條件的搜尋範例如下:

::

    > db.comedy.find({year:{$lt:1994}})
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill &
    Ted's Excellent Adventure", "year" : 1989 }
    { "_id" : ObjectId("4e9ebb478c02b838880ef413"), "name" : "Wayne's
    World", "year" : 1992 }

上面範例我們搜尋的條件是在year這個field(欄位)中值小於1994的,
$lt是MongoDB中的條件選項的其中一個，下方列出其他條件式運算:

::

    $lt - ' $lte - '
    $gte - '>='
    $ne - '!='
    $in - 'is in array'
    $nin - '! in array'

我們要如何執行'equal to' (==) 查詢? 下述的範例是搜尋year **符合1992** 的資料:

::

    > db.comedy.find({year:1992})
    { "_id" : ObjectId("4e9ebb478c02b838880ef413"), "name" : "Wayne's
    World", "year" : 1992 }

我們可以使用 regular expressions(正規運算式)查詢資料:

::

    > db.comedy.find({name:{$regex: /bill|steve/i}})
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill &
    Ted's Excellent Adventure" }

嘗試進階的regex搜索方式:

::

    > var names = ['bill', 'steve']
    > names = names.join('|');
    > var re = new RegExp(names, 'i')
    > db.comedy.find({name:{$regex: re}})
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill &
    Ted's Excellent Adventure" }

如果我只要取得結果的某些欄位就好:

::

    > db.comedy.find({year:{'$lt':1994}}, {name:true})
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill & Ted's Excellent Adventure" }
    { "_id" : ObjectId("4e9ebb478c02b838880ef413"), "name" : "Wayne's World" }
    > db.comedy.find({year:{'$lt':1994}}, {name:true})
    { "_id" : ObjectId("4e9ebb318c02b838880ef412"), "name" : "Bill & Ted's Excellent Adventure" }
    { "_id" : ObjectId("4e9ebb478c02b838880ef413"), "name" : "Wayne's World" }

上面範例會搜尋出comedy colletion 中year欄位中所有小於1994的資料, 只顯示符合資料
的name欄位。

像是 db.comedy.find({year:{'$lt':1994}}, {name:true, director:true}) 會搜尋出comedy
colletion 中year欄位中所有小於1994並且只顯示name以及director欄位的資料。

:command:`注意: 但是因為資料庫裡面沒有director欄位的資料，所以回傳的資料即使設定true也沒有director的資料。`

_id欄位為 **預設一定會回傳的欄位** ，若要隱藏則在條件後需要增加{_id:false}隱藏_id欄位

::

    > db.comedy.find({year:{'$lt':1994}}, {name:false})
    { "_id" : ObjectId("4ed201525bfc796ab22f9ee1"), "year" : 1989 }
    { "_id" : ObjectId("4ed201605bfc796ab22f9ee2"), "year" : 1992 }

MongDB
:command:`不能同時` 使用欄位的inclusion(列入) 和 exclusion(排除)運算, 例如你不能寫說我只
能顯示name欄位但是不要顯示year欄位, 你只能寫我要顯示name欄位, 或是我不要顯示
year欄位, 選一個。

MongoDB獨有的查詢語言(你可以參考 `dot notation <http://docs.mongodb.org/manual/>`_  。)


開一個新的collection 名稱為articles並寫入資料：

::

    > db.articles.insert({title:'The Amazingness of MongoDB', meta:
    {author:'Mike Vallely', date:1321958582668, likes:23, tags:
    ['mongo', 'amazing', 'mongodb']}, comments:[{by:'Steve',
    text:'Amazing article'}, {by:'Dave', text:'Thanks a ton!'}]})
    > db.articles.insert({title:'Mongo Business', meta:{author:'Chad
    Muska', date:1321958576231, likes:10, tags:['mongodb', 'business',
    'mongo']}, comments:[{by:'Taylor', text:'First!'}, {by:'Rob',
    text:'I like it'}]})
    > db.articles.insert({title:'MongoDB in Mongolia', meta:
    {author:'Ghenghiz Khan', date:1321958598538, likes:75, tags:
    ['mongo', 'mongolia', 'ghenghiz']}, comments:[{by:'Alex',
    text:'Dude, it rocks'}, {by:'Steve', text:'The best article
    ever!'}]})

要搜尋物件裏面的物件, 只要使用標準的JavaScript dot notation (點運算子)就可以存取物件
底下的任何一個子物件。
例如, 要查詢 meta.likes 物件, 你的作法如下:

::

    > db.articles.find({'meta.author':'Chad Muska'});
    > db.articles.find({'meta.likes':{$gt:10}})

**查詢陣列** 作法如下:

::

    > db.articles.find({'meta.tags':'mongolia'})

若是你要搜尋陣列內包含的物件，搜尋方式如下：

::

    > db.articles.find({'comments.by':'Steve'})

你可以指定要找陣列的哪個index(索引)的資料：

::

    > db.articles.find({'comments.0.by':'Steve'})

使用dot notation(點運算子)來陣列內物件的時候, 會有機會找到有相同key,但是不同value
的情況發生, 這個時候我們需要使用$elemMatch運算子。

補充資料：

$elemMatch

::

    // Document 1
    { "foo" : [
        {
            "shape" : "square",
            "color" : "purple",
            "thick" : false
        },
        {
            "shape" : "circle",
            "color" : "red",
            "thick" : true
        }
    ]}

    // Document 2
    { "foo" : [
        {
            "shape" : "square",
            "color" : "red",
            "thick" : true
        },
        {
            "shape" : "circle",
            "color" : "purple",
            "thick" : false
        }
    ] }

::

    > db.foo.find({"foo.shape": "square", "foo.color": "purple"});

若輸入上方指令由於資料內皆有符合條件，所以兩筆資料都會被顯示，這時候我們就需要
使用到$elemMatch 這個變數，範例如下：

::

    db.foo.find({foo: {"$elemMatch": {shape: "square", color: "purple"}}});

上述指令只會搜尋第一個物件中符合條件的資料顯示。

注意如果把數字用雙引號括起來, 那數字就是字串, 下面兩種查詢並不相同:

::

    > db.students.find({score:100})

::

    > db.students.find({score:'100'})

回傳資料型態均為字串, 因此如果你要使用的是數值, 那記得都要做轉換。

例如在Express.js中, 你可以這樣做:

::

    var score = +params.score;

params.score是一個 **字串** , 但是經由這樣的運算式, 變數score就是一個實際的JavaScript
number型態的資料, 可以在MonogoDB查詢中被可靠的使用。


MongoDB 可以支援javascript 運算式，範例如下：

::

    > db.comedy.find('this.year > 1990 && this.name != "The School of Rock"')

等同於使用MongoDB query operators:

    > db.comedy.find({year:{$gt:1990}, name:{$ne:'The School of Rock'}})

:command:`在mongoDB中使用JavaScript運算式比直接用原生的MongoDB operators 效率差。`

MongoDB有另外的運算子$where 可以使用，讓你可以使用像是SQL使用的WHERE方式,
使用範例如下：

::

    > db.comedy.find({$where: 'this.year > 2000'})
    { "_id" : ObjectId("4ed45fae2274c776f9179f89"), "name" : "The
    School of Rock", "year" : 2003 }

以及:

::

    > db.comedy.find({name:'The School of Rock', $where: 'this.year >
    2000'})
    { "_id" : ObjectId("4ed45fae2274c776f9179f89"), "name" : "The School of Rock", "year" : 2003 }

使用$where效率 **低於** 使用MongoDB運算子的搜尋效率, 所以除非使用MongoDB原生運算
子無法達到你搜尋需求的時候, 才使用JavaScript運算式或是$where。

查閱 `advanced MongoDB query operators <http://docs.mongodb.org/manual/reference/operators/>`_  頁面取得更多使用MongoDB運算子的使用方式。

Update data in MongoDB
^^^^^^^^^^^^^^^^^^^^^^^

我們嘗試在documents中加入新的field(欄位)：

::

    > db.comedy.update({name:"Bill & Ted's Excellent Adventure"},
    {'$set':{director:'Stephen Herek', cast:['Keanu Reeves', 'Alex
    Winter']}})

如何更新陣列中的值, 方法如下:

::

    > db.comedy.update({name:"Bill & Ted's Excellent Adventure"},
    {'$push':{cast:'George Carlin'}})
    > db.comedy.update({name:"Bill & Ted's Excellent Adventure"},
    {'$push':{cast:'Chuck Norris'}})

移除陣列中的資料方法如下:

::

    db.comedy.update({name:"Bill & Ted's Excellent Adventure"},
    {'$pull':{cast:'Chuck Norris'}})

更多update細節可以參考: `這裡 <http://docs.mongodb.org/manual/applications/update/>`_ 。


Delete data in MongoDB
^^^^^^^^^^^^^^^^^^^^^^^

從一個document中刪除一個欄位:

::

    > db.comedy.update({name:'Hulk'}, {$unset:{cast:1}})

所有collection裏面的document, 都要刪除這樣的一個欄位:

::

    > db.comedy.update({$unset:{cast:1}}, false, true)

false參數是給upsert選項用
:command:`( upsert是? )` , true參數是給
:command:`multiple選項` 用。
我們把multiple選項設為true, 表示我們要刪除collection裏面的所有符合條件的document項目。
不過這個用法不太妥當, 應該要使用 find()或是 findOne()選擇來執行update。

::

    > db.comedy.remove({name:'Hulk'})

上面命令會刪除所有name值是Hulk的所有documents。

清空collection中的所有documents資料：

::

    > db.comedy.remove()

上述命令就像是SQL的truncate命令, 沒有刪除collection, 而是reset這個collection。

delete/drop 一個collection :

::

    > db.comedy.drop()


刪除資料庫如下例：

先選擇database ,然後呼叫db.dropDatabase()。

::

    > use movies
    > db.dropDatabase()
    {"dropped" : "movies", "ok" : 1 }

count() 此函數可以使用在計算collection中的 **資料筆數** ：

::

    > db.comedy.count({})

也可以計算collection中 **特定條件的資料筆數** ，範例如下：

::

    > db.comedy.count({year:{$gt:1990})


進階參考:

MongoDB Documentation

1. `MongoDB Manua l <http://docs.mongodb.org/manual/contents/>`_
2. `Nightly generated PDF of the MongoDB Manua l <http://dl.mongodb.org/dl/docs/>`_

Further Reading

1. `MongoDB Introduction <http://www.mongodb.org/about/introduction/>`_
2. `SQL to Mongo Mapping Chart <http://docs.mongodb.org/manual/reference/sql-comparison/>`_
3. `MongoDB Advanced Queries <http://docs.mongodb.org/manual/reference/operators/>`_
4. `MongoDB Updating <http://docs.mongodb.org/manual/applications/update/>`_
5. `MongoDB Aggregation <http://docs.mongodb.org/manual/aggregation/>`_
6. `MongoDB Schema Design <http://docs.mongodb.org/manual/core/data-modeling/>`_
7. `MongoDB Indexes <http://docs.mongodb.org/manual/core/data-modeling/>`_
8. `MongoDB Cookbook <http://cookbook.mongodb.org/>`_
9. `MongoDB JavaScript API <http://api.mongodb.org/js/>`_

