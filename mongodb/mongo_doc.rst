MongoDB Document
------------------

MongoDB CRUD
===============

Introduction
^^^^^^^^^^^^^^

    MongoDB 利用BSON的方式儲存資料，每一筆資料中的欄位都是key 與 value的配對。

    Collections 是資料的集合，類似關聯似資料庫的table

    Query 提供條件進行操作，而query所給予的option 可以控制回傳的資料

    Data Modification 就是資料庫的CRUD

CRUD Concepts
^^^^^^^^^^^^^^^^

Read Operations Overview
""""""""""""""""""""""""""

    Query 大分為三個部份

        * query criteria

        * projection

        * cursor modifiler

    Query Behavior

        * 一次Query只對一個Collection做資料處理

        * 可以利用query對搜尋的資料做整理，限制與排序

        * 除非使用sort()，否則MongoDB不會自動幫你整理排序

        * 修改的Query跟先搜尋在修改資料的Query是一樣的

        * **In aggregation pipeline, the $match pipeline stage provides access to MongoDB queries.**

    Projection

        * 預設回傳所有吻合的資料

        * 利用projection 的搜尋結果回傳給client，可以減少網路流浪與程式負載

        * Except for excluding the _id field in inclusive projections, you cannot mix exclusive and inclusive projections.

        **exclusive？？inclusive？？**

    Cursor

        * 在MongoDB shell 中，20筆資料

        * 一個記憶體位置有一個指標指向一筆資料。

        ＊ 手動移動指標搜尋資料的方法請參照：
        **延伸未讀http://docs.mongodb.org/master/tutorial/iterate-a-cursor/**

        * Cursor Isolation:搜尋的時候cursor並不是獨立的，所以有可能在同時document被修改後，回傳的document不只一筆，要預防這種狀況請參照：
          延伸未讀 **http://docs.mongodb.org/master/faq/developers/#faq-developers-isolate-cursors**

        * 大部份第一次的 batches 大約會回傳101筆document，至少會佔1MB~4MB的記憶體，
          Batches size 的大小 : 延伸未讀 **http://docs.mongodb.org/master/reference/limits/#limit-bson-document-size**
          要修改Batches 請參照：
          延伸未讀 **http://docs.mongodb.org/master/reference/method/cursor.batchSize/#cursor.batchSize**
          延伸未讀 **http://docs.mongodb.org/master/reference/method/cursor.limit/#cursor.limit**

        * 若沒有設定index ，MongoDB必須將所有的document載入記憶體中，並且將所有的document 第1筆的 batches 回傳所有docuement

        * objsLeftInBatch()

          .. code-block :: javascript

            var myCursor = db.inventory.find();

            var myFirstDocument = myCursor.hasNext() ? myCursor.next() : null;

            myCursor.objsLeftInBatch();

        * MongoDB shell 中可以執行 db.runCommand( { cursorInfo: 1 } ) 得到下列幾項資訊

            * total number of open cursors(WTF？)

            * client 中 cursor 的大小

            * 啟動服務後 有幾次Timeout error

Query Optimezation
""""""""""""""""""""

    * Index 可以提高讀取資料效率，並且簡化MongoDB queries

    * Index 可以針對單個或多個欄位，若搜尋的欄位有Index的欄位，可以避免搜尋整個document
      延伸未讀 ： **http://docs.mongodb.org/master/core/indexes/**

      ..code-block :: javascript

        Mongo shell:

        db.inventory.ensureIndex( { type: 1 } )

        Example
        var typeValue = <someUserInput>;
        db.inventory.find( { type: typeValue } );

    * 分析Query performance :
      延伸未讀L **http://docs.mongodb.org/master/tutorial/analyze-query-plan/**

    * '$nin', '$ne'不管有沒有設定 index, 對於效率不會有太多的改善，而regular expressions($regex)不能使用index，
      **Queries that specify regular expression with anchors at the beginning of a string can use an index.**

    * Query中所有的field 是都包含在index的fiedl，並且回傳的所有field 都是index 裏面的field
      延伸未讀： **http://docs.mongodb.org/master/tutorial/create-indexes-to-support-queries/#indexes-covered-queries**

Query Plans
""""""""""""""

    * 可以利用 method  **explain()** 來觀察query plans 並且改進你的 indexing strategies.延伸未讀 **http://docs.mongodb.org/master/applications/indexes/**

    * 建立 Query 最佳化

        * 針對要最佳化的index 同時進行多次的query

        * 紀錄每次query 的 buffer 或 buffers

            * If the candidate plans include only ordered query plans, there is a single common results buffer.

            * If the candidate plans include only unordered query plans, there is a single common results buffer.

            * If the candidate plans include both ordered query plans and unordered query plans, there are two common results buffers, one for the ordered plans and the other for the unordered plans.

        * 停止unitest 並且檢查每一個 Query 的buffer 或　buffers

            * An unordered query plan has returned all the matching results; or

            * An ordered query plan has returned all the matching results; or

            * An ordered query plan has returned a threshold number of matching results:

        * 延伸未讀：
          **http://docs.mongodb.org/master/reference/glossary/#term-unordered-query-plan**
          **http://docs.mongodb.org/master/reference/glossary/#term-ordered-query-plan**

        * Query Plan 修訂

            * collection 有一千次的寫入

            * reindex 重新建立index

            * 刪除某個index

            * mongod 重新啟動

        * MongoDB 在2.5.5 新增了 Query Plan Cache Methods
          延伸未讀： **http://docs.mongodb.org/master/reference/method/js-plan-cache/**

        * indexex Filters :
          延伸未讀： **http://docs.mongodb.org/master/core/query-plans/#index-filters**

Distributed Queries
""""""""""""""""""""

    分佈式查詢：延伸未讀
    **http://docs.mongodb.org/master/core/distributed-queries/**

Write Operations
^^^^^^^^^^^^^^^^^^

Overview
""""""""""

    * MongoDB 有三種 write operations ： insert, update, remove

    * 修改可以使用update()或是save()
      細節 update() : http://docs.mongodb.org/manual/reference/method/db.collection.update/#db.collection.update
      save():http://docs.mongodb.org/manual/reference/method/db.collection.save/#db.collection.save

    * 延伸未讀 ： $isolated : http://docs.mongodb.org/manual/reference/operator/update/isolated/


Write Concern
================


Index Type
==============

    * 若沒有設定index，MongoDB 在搜尋時會將整個document 取出，造成效率低下

    * index 可以付帶query cirtieria 和 projection

Type
^^^^^^^

    Default _id

        Object _id

    Single Field

        針對某個欄位做index

    ..note ::

        Example:
        db.test.ensureIndex({score:1})

    Compound Index

        對多個欄位做index

    ..note ::

        Example:
        db.test.ensureIndex({userid:1, score:-1})

    Multikey Index

        * 針對陣列數值做index

        * MongoDB會自動建立Multikey index，不需要手動建立

        * Multikey搜尋時支援num, string兩種type，和巢狀document

        * shard keys 不能使用muti-key index


        ..note ::

            設定compound index 為{a:1, b:1}

            允許的Json

            {a: [1, 2], b: 1}

            {a: 1, b: [1, 2]}

            MongoDB若是已經設定上述compound index
            則
            {a: [1, 2], b: [1, 2]}這一筆資料會無法寫入並回傳 cannot index parallel arrays

    ..note ::

        Example:
        json {
            userid:'xxx',
            addr:[
                {zip:"10036"},
                {zip:'97455'}
            ]
        }

        Create Index

        db.test.ensureIndex({'addr.zip':1})

    Geospatial Index

        支援2d 地理空間資料index(還需要研究，這瞎小)

    Text Index

        MongoDB 支援text index，
        These text indexes do not store language-specific stop words
        (e.g. “the”, “a”, “or”) and stem the words in a collection to only store root words.

        * Text Index 不區分大小寫，Text Index 內容可以是String, array或是其他field(dict)

        * 在建立一個text index之前要先Enable Text Search

        * 一個collection 只可以有一個text index

        * Specify a Language for Text Index(針對Text Index 語言設定)

            * 預設的語言為English

            ..note ::

                Example:

                設定為單班牙語系

                db .test.ensureIndex(
                    {content:'text'},
                    {default_language:"spnish"}
                )

                設定多國語系

                Json Example:

                { _id: 1, idioma: "portuguese", quote: "A sorte protege os audazes" }
                { _id: 2, idioma: "spanish", quote: "Nada hay más surreal que la realidad." }
                { _id: 3, idioma: "english", quote: "is this a dagger which I see before me" }

                Example:

                db.test.ensureIndex(
                    {quote:'text'},
                    {language_override:'idioma'}
                )

                db.test.runCommand( "text", { search: "que", language: "spanish" } )



        ..note ::

            Example:

            可以針對某些field 設定text index

            db.test.ensureIndex(
                {
                    subject:'text',
                    content:'text'
                }
            )

            也可以針對所有field 使用"$**" 特殊字元設定所有的 text index

            db.test.ensureIndex(
                {
                    "$**":"text",
                    name : "TextIndex"
                }
            )

        ..note ::

            Enable Text Search :

            http://docs.mongodb.org/manual/tutorial/enable-text-search/

        * 每個Collection只能建立一個 Text Index

        * Text Index 需要付出的載體需求與效率注意事項

            * text indexes change the space allocation method for all future record allocations in a collection to usePowerOf2Sizes.
              (變更儲存方法，詳細延伸未讀:http://docs.mongodb.org/manual/reference/command/collMod/#usePowerOf2Sizes)

            * Text index 很佔記憶體空間

            * text index 和 巨大multi-key index是相似的，都會佔用很大的記憶體空間

            * 建立一個大的Text Index 會有很大的限制，
              延伸未讀：http://docs.mongodb.org/manual/reference/ulimit/

            * 會影響insert的效率，因為每次新增時都必須在每個新的document 加入unique post-stemmed word

            * Text Index 不要只有儲存片語或是關於document 的相關資訊？較小的Text Index 可以在RAM中直接被搜尋自然提高效率。

        * Text Search 可以對文件中的部份字串進行搜尋

    Hashed Index

        * MongoDB 支援 hashed index，但是不支援Multikey index，有支援sharding，

        * 可以接受equally query 但是不能接受 range

        ..note ::

              延伸未讀
            hashed shard key :
            http://docs.mongodb.org/manual/core/sharding-shard-key/#sharding-hashed-sharding

            shard a Collection Using a Hashed Shard Key:
            http://docs.mongodb.org/manual/tutorial/shard-collection-with-a-hashed-shard-key/


Index Properties
^^^^^^^^^^^^^^^^^^^

    Unique Indexes

        * MongoDB unique的預設值是 flase

        * 若針對compound index 中的某一個直設定為unique, MongoDB 會將這個組合做unique ，而不是對單一的值(If you use the unique constraint on a compound index, then MongoDB will enforce uniqueness on the combination of values rather than the individual value for any or all values of the key.)

        * 對某個field設定unique, 第1筆空值資料會補Null值，但是在第2筆空值時，則會出現duplicate key error, 你可以結合Sparse index 來避免這些錯誤。

        * 不能對hashed index 設定unique prototype

        ..note ::

            Example:

            db.test.ensureIndex({"user_id":1}, {unique:true})

    Sparse Indexes

Search String content for Text
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    延伸未讀：http://docs.mongodb.org/manual/tutorial/search-for-text/
