
MongoDB Indexing Strategies
---------------------------------

**作者:Tomas**


1. 如何有效的規劃資料庫的結構:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    1. 這個APP需要的資料為何?
    2. 每個欄位的搜尋頻率多寡
    3. 哪一個index在APP會有最多次機會被搜尋。

2. 規劃KEY值
^^^^^^^^^^^^^^^

    1. 在規劃MongoDB的index的Key值的時候，主要是一句你的query的搜尋條件key值有幾個，
    以減少query的次數為優先。

    2. MongoDB支援Compound indexes:複合索引 [#]_ ，在insert 、 find 、
    update 、remove(CRUD)都可以利用複合索引進行資料庫控制。

    3. 可以在搜尋同時設定你所需要回傳的資料，如下例

::

    # db.test.insert({ 'user': 'Thomas'})

    #db.test.find({ 'user' : 'Thomas'}, {'user' : 1, '_id' : 0})


\
    4. 若是值的型態是array(object)，則該筆資料並不支援cover query。
    5. 建立一個indexes的 result再利用sort()來控制Covered Query，如下例:

::

    #db.test.insert({ a: 1, b: 1, c: 1, d: 1 })

    #db.collection.find().sort( { a:1 } )
    #db.collection.find().sort( { a:1, b:1 } )

    #db.collection.find( { a:4 } ).sort( { a:1, b:1 } )
    #db.collection.find( { b:5 } ).sort( { a:1, b:1 } )

    #db.collection.find( { a:5 } ).sort( { b:1, c:1 } )

    #db.collection.find( { a:5, c:4, b:3 } ).sort( { d:1 } )
    #db.collection.find( { a: { $gt:4 } } ).sort( { a:1, b:1 } )
    #db.collection.find( { a: { $gt:5 } } ).sort( { a:1, b:1 } )
    #db.collection.find( { a:5, b:3, d:{ $gt:4 } } ).sort( { c:1 } )
    #db.collection.find( { a:5, b:3, c:{ $lt:2 }, d:{ $gt:4 } } ).sort( { c:1 } )

\
    6. `db.collection.totalIndexSize() <http://docs.mongodb.org/manual/reference/method/db.collection.totalIndexSize/#db.collection.totalIndexSize>`_ :可以利用此指令查詢此collection所佔的byte大小

    7. Ensure Indexes Fit RAM

::

    > db.collection.totalIndexSize()
    4294976499

在我執行MongoDB的指令時必須確保有足夠的暫時記憶體可以存放資料，
上述範例中做的搜尋有4.3G的大小，若是剩餘的佔存記憶體不夠，則會占用
一般記憶體，則會影響到效率，要多加注意。而有一部分的indexes原本就存放
在佔存記憶體中，則不需要計算在內。

See also
For additional.collection statistics [#]_ , use.collStats or db.collection.stats(). [#]_

3. 利用搜尋條件來過濾掉不必要的資料，增加回傳資料的準確性。
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. 若是你的APP是一個重度寫入資料庫的模式，更需要注意如何設計indexes，否則對於效率會有極大的影響。

2. 一個不常使用的index，將會影響搜尋或是修改的效率。

3. 若collection的資料是有大量的indexes與重度寫入的狀態，你必須要社記一個可以減少執行query次數的資料架構。

4. 你在搜尋的條件設定會影響效率問題。



4.  參考資料:
^^^^^^^^^^^^^^^^^^^

http://docs.mongodb.org/manual/applications/indexes/

5. 備註資料:
^^^^^^^^^^^^^^^

.. [#] : `Compound indexes:複合索引 <http://docs.mongodb.org/manual/core/indexes/#index-type-compound>`_
.. [#] : `.collection statistics <http://docs.mongodb.org/manual/reference/collection-statistics/>`_
.. [#] :  `collStats <http://docs.mongodb.org/manual/reference/command/collStats/#dbcmd.collStats>`_ , `db.collection.stats() <http://docs.mongodb.org/manual/reference/method/db.collection.stats/#db.collection.stats>`_

........................................................................

Mongodb-pymongo初階教學
-----------------------

`pymongo <http://api.mongodb.org/python/current/>`_ 是一個


**作者:Tomas**

1.install （安裝）
^^^^^^^^^^^^^^^^^^

::

    $sudo yum install mongodb libmongodb mongodb-server

2.start（開始）
^^^^^^^^^^^^^^^

::

    $sudo service mongod start
    $chkconfig mongod on

3.test_pymongo.py（測試）:
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block :: python

    # -*- coding: utf-8 -*-
    from pymongo import Connection
    from time import time, strftime, localtime
    from random import randint
    class PyConn(object):

        def __init__(self, host, port):
            self.dbname=''
            self.collname=''
            try:
                self.conn = Connection(host, port)
            except  Error:
                print 'connect to %s:%s fail' %(host, port)
                exit(0)

        def __del__(self):
            self.conn.close()

        def setDB(self, dbname):
            # 這種[]獲取方式同样适用於shell,下面的collection也一样
            #db 類型<class 'pymongo.database.Database'>
            self.db = self.conn[dbname]
            self.dbname=dbname

        def setCollection(self, collection):
            if not self.db:
                print 'don\'t assign database'
                exit(0)
            else:
                self.coll = self.db[collection]
                self.collname=collection

        def find(self, \*\*query): #查詢
            try:
                #result類型<class 'pymongo.cursor.Cursor'>
                if not self.coll:
                    print 'don\'t assign collection'
                else:
                    result = self.coll.find(query)
            except NameError:
                print 'some fields name are wrong in ',query
                exit(0)
            return result

        def find_one(self, \*\*query): #查詢
            print query
            try:
                #result類型<class 'pymongo.cursor.Cursor'>
                if not self.coll:
                    print 'don\'t assign collection'
                else:
                    result = self.coll.find_one(query)
                    print result
            except NameError:
                print 'some fields name are wrong in ',query
                exit(0)
            return result

        def insert(self, \*\*data):   #新增
            #insert會返回新插入數據的_id
            self.coll.insert(data)

        def remove(self, \*\*data): #刪除
            self.coll.remove(data)

        def update(self, data, \*\*setdata): #修改
            if type(data) is not dict or type(setdata) is not dict:
                print 'the type of update and data isn\'t dict'
                exit(0)
            #update無返回值
            self.coll.update(data,{'$set':setdata})

執行測試:

::

    $python test_pymongo.py

參考資料
""""""""""""""

    http://rritw.com/a/shujuku/20120626/176249.html
