Replication
-------------------------------------------------------------------

Deploy a Replica Set
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    **建立三個測試的Mongo Replication**
    
        先停止MongoDB的服務
        
        ::
        
            $ sudo /etc/init.d/mongodb stop

        修改/etc/hosts 中的127.0.0.1 localhost 加入
        
        ::
        
            127.0.0.1 localhost m1.example.net m2.example.net m3.example.net
        
        開3個port 模擬三台電腦的MongoDB
        
        ::
        
            $ mongod --port 27017 --dbpath /srv/mongodb/rs0-0 --replSet rs0 --smallfiles --oplogSize 128
            $ mongod --port 27018 --dbpath /srv/mongodb/rs0-1 --replSet rs0 --smallfiles --oplogSize 128
            $ mongod --port 27019 --dbpath /srv/mongodb/rs0-2 --replSet rs0 --smallfiles --oplogSize 128

    **Shell mode**
    
    ::
    
        rsconf = {
           _id: "rs0",
           members: [
                      {
                       _id: 0,
                       host: "m1.example.net:27017"
                      }
                    ]
         }
         
    **增加其他MongoDB**
    
    ::
    
        rs.add("m2.example.net:27018")
        rs.add("m3.example.net:27018")

    **查看狀態**
    
    ::
    
         $ rs.status()

