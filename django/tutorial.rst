Django Tutorial
-------------------

    架構

    ::

        mysite/
            manage.py
            mysite/
                init__.py
                settings.py
                urls.py
                wsgi.py


    Create a new project

    ::

        $ django-admin.py startproject projectname

    Create a file name it views.py

    ::

        $ cd projectname

        $ touch views.py

    views.py

    .. code-block :: python

        from django.http import HttpResponse

        def hello(request):
            return HttpResponse('Hello World!!')

    create a new app

    ::

        $ python manage.py startapp polls #python manage.py startapp appname

    app 資料結構

    ::

        polls/
            __init__.py
            models.py
            tests.py
            views.py

    polls/models.py

    .. code-block :: python

        from django.db import models
        class Poll(models.Model):
            question = models.CharField(max_length=200)
            pub_date = models.DateTimeField('date published')

        class Choice(models.Model):
            poll = models.ForeignKey(Poll)
            choice_text = models.CharField(max_length=200)
            votes = models.IntegerField(default=0)

    setting.py

    ::

        INSTALLED_APPS = (
            'django.contrib.auth',
            'django.contrib.contenttypes',
            'django.contrib.sessions',
            'django.contrib.sites',
            'django.contrib.messages',
            'django.contrib.staticfiles',
            # Uncomment the next line to enable the admin:
            # 'django.contrib.admin',
            # Uncomment the next line to enable admin documentation:
            # 'django.contrib.admindocs',
            'polls'
           )

    Now Django knows to include the polls app.

    ::

        $ python manage.py sql polls

    Now, run syncdb again to create those model tables in your database:

    ::

        $ python manage.py syncdb

    Now, let’s hop into the interactive Python shell and play around with the free API Django gives you. To invoke the Python shell, use this command:

    ::

        $ python manage.py shell

        >>> from polls.models import Poll, Choice   # Import the model classes we just wrote.

        # No polls are in the system yet.
        >>> Poll.objects.all()
        []

        # Create a new Poll.
        # Support for time zones is enabled in the default settings file, so
        # Django expects a datetime with tzinfo for pub_date. Use timezone.now()
        # instead of datetime.datetime.now() and it will do the right thing.
        >>> from django.utils import timezone
        >>> p = Poll(question="What's new?", pub_date=timezone.now())

        # Save the object into the database. You have to call save() explicitly.
        >>> p.save()

        # Now it has an ID. Note that this might say "1L" instead of "1", depending
        # on which database you're using. That's no biggie; it just means your
        # database backend prefers to return integers as Python long integer
        # objects.
        >>> p.id
        1

        # Access database columns via Python attributes.
        >>> p.question
        "What's new?"
        >>> p.pub_date
        datetime.datetime(2012, 2, 26, 13, 0, 0, 775217, tzinfo=<UTC>)

        # Change values by changing the attributes, then calling save().
        >>> p.question = "What's up?"
        >>> p.save()

        # objects.all() displays all the polls in the database.
        >>> Poll.objects.all()
        [<Poll: Poll object>]


    修改 polls/models.py

    .. code-block :: python

        #-*- coding:utf-8 -*-
        from django.db import models
        import datetime
        from django.utils import timezone

        # Create your models here.
        class Poll(models.Model):
            question = models.CharField(max_length=200)
            pub_date = models.DateTimeField('date published')

            def __unicode__(self):  # Python 3: def __str__(self):
                return self.question
            def was_published_recently(self):
                return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

        class Choice(models.Model):
            poll = models.ForeignKey(Poll) #models.ForeignKey?? 有何功用
            choice_text = models.CharField(max_length=200)
            votes = models.IntegerField(default=0)

            def __unicode__(self):  # Python 3: def __str__(self):
                return self.choice_text

    執行 python manage.py shell

    ::

        >>> from polls.models import Poll, Choice

        # Make sure our __unicode__() addition worked.
        >>> Poll.objects.all()
        [<Poll: What's up?>]

        # Django provides a rich database lookup API that's entirely driven by
        # keyword arguments.
        >>> Poll.objects.filter(id=1)
        [<Poll: What's up?>]
        >>> Poll.objects.filter(question__startswith='What')
        [<Poll: What's up?>]

        # Get the poll that was published this year.
        >>> from django.utils import timezone
        >>> current_year = timezone.now().year
        >>> Poll.objects.get(pub_date__year=current_year)
        <Poll: What's up?>

        # Request an ID that doesn't exist, this will raise an exception.
        >>> Poll.objects.get(id=2)
        Traceback (most recent call last):
            ...
        DoesNotExist: Poll matching query does not exist. Lookup parameters were {'id': 2}

        # Lookup by a primary key is the most common case, so Django provides a
        # shortcut for primary-key exact lookups.
        # The following is identical to Poll.objects.get(id=1).
        >>> Poll.objects.get(pk=1)
        <Poll: What's up?>

        # Make sure our custom method worked.
        >>> p = Poll.objects.get(pk=1)
        >>> p.was_published_recently()
        True

        # Give the Poll a couple of Choices. The create call constructs a new
        # Choice object, does the INSERT statement, adds the choice to the set
        # of available choices and returns the new Choice object. Django creates
        # a set to hold the "other side" of a ForeignKey relation
        # (e.g. a poll's choices) which can be accessed via the API.
        >>> p = Poll.objects.get(pk=1)

        # Display any choices from the related object set -- none so far.
        >>> p.choice_set.all()
        []

        # Create three choices.
        >>> p.choice_set.create(choice_text='Not much', votes=0)
        <Choice: Not much>
        >>> p.choice_set.create(choice_text='The sky', votes=0)
        <Choice: The sky>
        >>> c = p.choice_set.create(choice_text='Just hacking again', votes=0)

        # Choice objects have API access to their related Poll objects.
        >>> c.poll
        <Poll: What's up?>

        # And vice versa: Poll objects get access to Choice objects.
        >>> p.choice_set.all()
        [<Choice: Not much>, <Choice: The sky>, <Choice: Just hacking again>]
        >>> p.choice_set.count()
        3

        # The API automatically follows relationships as far as you need.
        # Use double underscores to separate relationships.
        # This works as many levels deep as you want; there's no limit.
        # Find all Choices for any poll whose pub_date is in this year
        # (reusing the 'current_year' variable we created above).
        >>> Choice.objects.filter(poll__pub_date__year=current_year)
        [<Choice: Not much>, <Choice: The sky>, <Choice: Just hacking again>]

        # Let's delete one of the choices. Use delete() for that.
        >>> c = p.choice_set.filter(choice_text__startswith='Just hacking')
        >>> c.delete()
