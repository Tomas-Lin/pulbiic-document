Basic Skill
-------------

系統資料
^^^^^^^^^

    Linux: cruchbang(Debian)

    nodejs: 0.10.26

    tarball: http://nodejs.org/dist/v0.10.26/node-v0.10.26.tar.gz

    express: 4.0.0(3.5.X與4.0.0是大改版！需注意版本號碼)

    ::

        npm install npm install express@4.0.0


Heroku | 10 Habits of a Happy Node Hacker
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    npm init 初始化APP

    ::

        mkdir my-node-app
        cd my-node-app
        npm init

    紀錄所有的相關性套件

    ::

        npm install tool-package --save

    設定start script in package.json

    ::

        "scripts": {  "start": "node index.js"}

    設定test script in package.json

    ::
        "scripts": {  "test": "mocha"}

    將相依性套件排除再版本控制系統之外

    ::

        echo node_modules >> .gitignore

    Use environment variables to configure npmFrom (還沒測試搞懂)

       the npm config docs:Any environment variables that start with npm_conf
       ig_ will be interpreted as a configuration parameter. For example, putting npm_config_foo=bar in your environment will set the foo configuration parameter to bar. Any environment configurations that are not given a value will be given the value of true. Config values are case-insensitive, so NPM_CONFIG_FOO=bar will work the same.As of recently, app en
       vironment is available in all Heroku builds. This change gives node users on Heroku control over their npm configuration without having to make changes to application code. Habit #7 is a perfect example of this approach.


    Bring your own npm registryThe public npm registry has seen immense(忘記heroku怎麼搞 所以有空再測試)

        growth in recent years, and with that has come occasional instability. As a result, many node users are seeking alternatives to the public registry, either for the sake of speed and stability in the development and build cycle, or as a means to host private node modules.A number or alter
         native npm registries have popped up in recent months. Nodejitsu and Gemfury offer paid private registries, and there are some free alternatives such as Mozilla's read-only S3/CloudFront mirror and Maciej Małecki's European mirror.Configuring your Heroku node app
         to use a custom registry is easy:

    控管dependencies

    ::

        cd my-node-app
        npm outdated

    利用npm scripts 執行相關程式

    ::

        {
            "name": "my-node-app",
            "version": "1.2.3",
            "scripts": {
                "preinstall": "echo here it comes!",
                "postinstall": "echo there it goes!",
                "start": "node index.js",
                "test": "tap test/*.js"
            }
        }

    OR

    ::

        {
            "scripts": {
                "postinstall": "npm run build && npm run rejoice",
                "build": "grunt",
                "rejoice": "echo yay!",
                "start": "node index.js"
            }
        }

    多嘗試新事物

    Have fun!!

    參考資料： https://blog.heroku.com/archives/2014/3/11/node-habits

package.json
^^^^^^^^^^^^^^

    * 設定APP的設定檔案

    demo

    package.json

    .. code-block :: json

        {
            "name": "node_mssql_demo",
            "version": "0.0.1",
            "description": "Nodejs connect to MsSQL",
            "main": "app.js",
            "dependencies": {
                "mssql": "~0.5.3",
                "node-mssql-connector": "^0.2.3",
                "tds": "~0.1.0",
                "express": "^4.3.1",
                "jade": "^1.3.1",
                "method-override": "^1.0.2",
                "body-parser": "^1.2.1",
                "mocha": "^1.19.0"
            },
            "devDependencies": {},
            "scripts": {
                "test": "mocha",
                "start": "node app.js"
            },
            "keywords": [
                "Node.js",
                "MsSQL"
            ],
            "author": "Tomas",
            "license": "ISC"
        }



