unittest
----------

Locust
^^^^^^^^

    Loading test tool


Install
""""""""""

    ::

        pip install locustio

    若要測試多程序的則安裝pyzmq

    ::

        pip install pyzmq

        or

        easy_install pyzmq

    start sample

    .. code-block :: python

        from locust import HttpLocust, TaskSet

        def login(I):
            I.client.post('/login', {"username": "ellen_key", "password":"education"})

        def index(I):
            I.client.get('/')

        def profile(I):
            I.client.get('/profile')


        class UserBehavior(TaskSet):
            tasks = {index: 2, profile: 1}

            def on_start(sself):
                login(self)

        class WebsiteUser(HttpLocust):
            task_set = UserBehavior
            min_wait = 5000
            max_wait = 9000
