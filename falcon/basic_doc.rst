Falcon Basic Doc
------------------------------------------------------------------

Install
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

        ::
        
            $ pip install cython falcon
          
        Get Demo
        
        .. code-block :: python
        
            import falcon
            class ThingsResource:
                def on_get(self, req, resp):
                    #~ Handle GET requests
                    resp.status = falcon.HTTP_200
                    resp.body = ("GET:Hello World!!!")
                
                        
                    
            app = api = falcon.API()

            things = ThingsResource()

            api.add_route('/things', things)
            
        POST Demo
        
        .. code-block :: python
        
            import falcon
            import json

            class ThingsResource:
                def on_get(self, req, resp):
                    #~ Handle GET requests
                    resp.status = falcon.HTTP_200
                    resp.body = ("GET:Hello World!!!")
                    
                    
                def on_post(self, req, resp):
                    try:
                        raw_json = req.stream.read()
                    except Exception as ex:
                        raise falcon.HTTPError(falcon.HTTP_400,
                            'Error',
                            ex.message)
                            
                    try:
                        result_json = json.loads(raw_json, encoding='utf-8')
                    except ValueError:
                        raise falcon.HTTPError(falcon.HTTP_400,
                            'Malformed JSON',
                            'Copuld not decode the request body. \
                            The JSON was incorrect')
                    
                    resp.status = falcon.HTTP_202
                    resp.body = json.dumps(result_json, encoding='utf-8')
                    
            app = api = falcon.API()

            things = ThingsResource()

            api.add_route('/things', things)

        若輸入的值並非json回傳錯誤訊息為{"title":"Malformed JSON", 
        "content" : "Copuld not decode the request body.
        The JSON was incorrect"，代碼為400。
        
        

        
參考資料
------------------------------------------------------------------

    官網 ： http://falconframework.org/
    
    github : https://github.com/racker/falcon/blob/master/README.md#install
    
    demo : http://www.giantflyingsaucer.com/blog/?p=4342
