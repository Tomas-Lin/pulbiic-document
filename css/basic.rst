Basic
-------------------------------

紀錄一些使用方法：
漸層
::

    filter: progid: DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='#ff7f2a', endColorstr='#ff6600');

margin

::

    margin:5px 5px 5px 5px;
            上  左  下   右

google chrome 要縮小字體有限制到12px，若要再小必須加上一行宣告

::

    -webkit-text-size-adjust:none;

overflow

::

    overflow:visible; #不管有沒有超出皆顯示 scroll bar
    overflow:hidden; #隱藏 scroll bar
    overflow:auto; #顯示 scroll bar
    overflow:scroll; #若有超出才顯示 scroll bar

display

::

    display:block; # 區塊，元素會以區塊方式呈現，除非設定 position 或 float。
    display:none; # 區塊，元素會隱藏
    display:inline; # 所有文字或圖片均不換行，也就是全部都會是同一行的意思

font-weight

::

    font-weight:normal;
    font-weight:bold;
    font-weight:bolder;


Animations

css

::

    div{
    width:100px;
    height:100px;
    background:red;
    animation:myfirst 5s;
    -webkit-animation:myfirst 5s; /* Safari and Chrome */
    }

    @keyframes myfirst{
    from {background:red;}
    to {background:yellow;}
    }

    @-webkit-keyframes myfirst /* Safari and Chrome */{
    from {background:red;}
    to {background:yellow;}
    }

html

::

    <p><b>Note:</b> This example does not work in Internet Explorer 9 and earlier versions.</p>
    <div></div>

也可以控制顏色

css

::

    div
    {
    width:100px;
    height:100px;
    background:red;
    animation:myfirst 5s;
    -webkit-animation:myfirst 5s; /* Safari and Chrome */
    }

    @keyframes myfirst
    {
    0%   {background:red;}
    25%  {background:yellow;}
    50%  {background:blue;}
    100% {background:green;}
    }

    @-webkit-keyframes myfirst /* Safari and Chrome */
    {
    0%   {background:red;}
    25%  {background:yellow;}
    50%  {background:blue;}
    100% {background:green;}
    }

html

::
    <div></div>


圓角

::

    -webkit-border-radius: 2px;
    -moz-border-radius:2px;
    border-radius:2px;

** 單一角 **
** 其他的當然就是運用 top、bottom、left、right 混搭 **

::

    border-top-right-radius
    -moz-border-top-right-radius
    -webkit-border-top-right-radius

陰影：參數依序為：X偏移值、Y偏移值、柔化的半徑、顏色，可以運用負號控制左右上下。

::

    -webkit-box-shadow: 0px 2px 3px #F2F2F2;
    -moz-box-shadow: 0px 2px 3px #F2F2F2;
    box-shadow: 0px 2px 3px #F2F2F2;

文字陰影：參數依序為：X偏移值、Y偏移值、柔化的半徑、顏色，可以運用負號控制左右上下。

其中文字陰影都可以連續疊加，但是到時後要控制得宜，不然會變很low..

::

    text-shadow: 0px 1px 1px #fff;


調整透明：百分比:80%

::

    filter: alpha(opacity=80);
    -moz-opacity:0.8;
    opacity:0.8;

多層背景：也就是直接,後面再接url即可，但IE使用了後會直接消失。

::

    background:url(../images/logo.png) left top no-repeat,url(../images/logo.png) left top no-repeat;

旋轉：因為這功能很少會用到（或許配合jQuery?），所以僅是紀錄，IE那個旋轉參數我沒有仔細去研究，其他應該看的懂是角度

::

    -webkit-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation = 3);

input表單打字時有框

::

    input:focus, textarea:focus { border:1px solid #f90; }


參考網站
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    CSS3 Mozilla 官方 : https://developer.mozilla.org/en-US/docs/Web/CSS

    CSS3 : http://css3.bradshawenterprises.com/

    CSS的Demo : http://ezcshi.pixnet.net/blog/post/36464540
