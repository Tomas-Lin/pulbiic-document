Sass
---------------

Installing & Quickly Start
""""""""""""""""""""""""""""

    ::
        $ gem install sass

    input.scss

    .. code-block :: sass

        #main p {
          color: #00ff00;
          width: 97%;

          .redbox {
            background-color: #ff0000;
            color: #000000;
          }
        }

    ::

        $ sass input.scss output.css


Variables
""""""""""""""

	在sass 中可以設定變數

	.. code-block :: sass

		$font-stack : Helvetica, sans-serif
		$primary-color: #333

		body{
			font : 100% $font-stack;
			color: $primary-color;
		}

Nesting
""""""""

	sass 會依照scss中的HTML的結構編譯出相對應的CSS

	.. code-block :: sass

		nav{
				ul{
	            margin:0;
	            padding:0;
	            list-style:none;
	        }

	        li{
	            display:inline-block;
	        }

	        a{
	            display:block;
	            padding:6px 12px;
	            text-decoration:none;
	        }
	    }

	編譯後

	.. code-block :: css

		nav ul {
		  margin: 0;
		  padding: 0;
		  list-style: none;
		}
		nav li {
		  display: inline-block;
		}
		nav a {
		  display: block;
		  padding: 6px 12px;
		  text-decoration: none;
		}


Import
"""""""""""

	scss可以引入另外一個scss的檔案，進行模組化切割，但是缺點就是使用@import 引入的時候
	它會創建一個HTTP 請求。


Inheritance
"""""""""""""

	Inheritance.scss

	.. code-block :: sass

		.message{
			border:1px solid #ccc;
			padding:10px;
			color:#333;
		}

		.sucess{
			@extend .message;
			border-color:green;
		}

		.error{
			@extend .message;
			border-color:red;
		}

		.warning{
			@extend .message;
			border-color:yellow;
		}

	Inheritance.css

	.. code-block :: css

		.message, .sucess, .error, .warning {
		  border: 1px solid #ccc;
		  padding: 10px;
		  color: #333;
		}

		.sucess {
		  border-color: green;
		}

		.error {
		  border-color: red;
		}

		.warning {
		  border-color: yellow;
		}


Operators
"""""""""""""

	Operators.scss

	.. code-block :: sass

		.container{
			width:100%;
		}

		article[role="main"]{
			float:left;
			width:600px/960px * 100%;
		}

		aside[role='complimentart']{
			float:right;
			width:300px/960px * 100%;
		}

	Operators.css

	.. code-block :: css

		.container {
		  width: 100%;
		}

		article[role="main"] {
		  float: left;
		  width: 62.5%;
		}

		aside[role='complimentart'] {
		  float: right;
		  width: 31.25%;
		}

Referencing Parent Selectors :&
"""""""""""""""""""""""""""""""""

	selestor.scss

	.. code-block : sass

		a{
			font-weight:bold;
			text-decoration:none;
			&:hover{
				text-decoration:underline;
			}
			body.firefox & {
				font-weight:normal;
			}
		}

	selestor.css

	.. code-block : css

		a {
		  font-weight: bold;
		  text-decoration: none;
		}
		a:hover {
		  text-decoration: underline;
		}
		body.firefox a {
		  font-weight: normal;
		}

