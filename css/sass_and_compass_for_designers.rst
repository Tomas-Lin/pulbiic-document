Sass and compass for Designers
---------------------------------

Chapter 1.
^^^^^^^^^^^

    使用 Sass and Compass 的寫作風格可以讓 Css更加靈活與易於維護，並且較以往直接攥寫Css更加快速。

    Css 是一種宣告，並不是一種程式語言

    Why not just write CSS?

        若你不會使用CSS，Sass and Compass無法填補你這塊空白！

        Sass可以協助攥寫CSS時更快速與容易維護。

    Why you should use Sass and Compass?

        定義變數(只能定義一個變數一次)

            在一個Css中同樣的顏色可能會使用多次，定義一個變數代表之後，若以後需要修改也只需要
            修改該變數的值。

        .. note ::

            編譯在Sass中代表的只是從scss檔案轉換為css檔案的過程。


        自動RGBA顏色值和轉換

            支援RGBA與HSLA。

            example

            .. code-block :: sass

                .color-me-bad{
                    color:#11c909;
                    color:ragb(17, 201, 9, 0.9);
                }

            Sass 可以在hex與RGBA的值互相共用

            example

            .. code-block :: sass

                .color-me-good{
                    color:$green;
                    color:rgba($green, 0.9);
                }

        .. note ::

            RGBA代表 Red(紅色) Green(綠色) Blue(藍色) 和Alpha的色彩空間。有時也被寫成ARGB。

            Alpha通道一般用作不透明度參數，若像素的Alpha通道為0%，則是完全透明(即是看不見的)，

            HSL與HSV都是將RGB的色彩模型中在圓柱座標系中的表示法。

            HSL為 Hue(色相) saturation(飽和度) Lightness(亮度)，又稱之為HLS。

            HSV為 Hue(色相) saturation(飽和度) value(亮度)，又稱之為HSB。

            資料來源 :

            http://zh.wikipedia.org/wiki/HSL%E5%92%8CHSV%E8%89%B2%E5%BD%A9%E7%A9%BA%E9%97%B4

            http://zh.wikipedia.org/wiki/RGBA

        跨瀏覽器設定

            圓角example

            .. code-block :: sass

                .rounded{
                    -webkit-border-radius: 4px;
                    -moz-border-radius: 4px;
                    -ms-border-radius: 4px;
                    -o-border-radius: 4px;
                    border-radius: 4px;
                }

            Mixins example

            .. code-block :: sass

                @mixin border-radius($radius){
                    -webkit-border-radius: $radius;
                    -moz-border-radius: $radius;
                    -ms-border-radius: $radius;
                    -o-border-radius: $radius;
                    border-radius: $radius;
                }

                .rounded{
                    @include border-radius(4px)
                }

        Nesting rules(內嵌規則)

            nesting example

            .. code-block :: sass

                nav{
                    a{
                        color:red;
                        &:hover{
                            color:green;
                        }
                        &:visited{
                            color:blue;
                        }
                    }
                }

            編譯後

            .. code-block :: css

                nav a {
                  color: red;
                }
                nav a:hover {
                    color: green;
                }
                nav a:visited {
                    color: blue;
                }

            .. note ::

                &:hover 會自動編譯成為nav a:hover {color: green;}。

        Media queries

            example

            .. code-block :: sass

                @media only screen and (mini-width:280px) and (max-width:479px){
                    .h1{
                        font-size:1.1em;
                    }
                }

                @media only screen and (mini-width:480px) and (max-width:599px){
                    .h1{
                        font-size:1em;
                    }
                }

                @media only screen and (mini-width:600px) and (max-width:767px){
                    .h1{
                        font-size:0.9em;
                    }
                }

            media and mixin example

            .. code-block :: sass

                @mixin MQ($size){
                    @if $size == xs {
                        $mini-w = 280px;
                        $max-w = 479px;
                    }
                    @if $size == s {
                        $mini-w = 480px;
                        $max-width=599px;
                    }
                    @if $size == M {
                        $mini-w = 600px;
                        $max-w = 767px;
                    }

                    @media only screen and (mini-width:$mini-w) and (max-width:$max-w)
                }

                h1{
                    @include MQ(xs){
                        font-size:1.1em;
                    }

                    @include MQ(s){
                        font-size:1em;
                    }

                    @include MQ(M){
                        font-size:0.9em;
                    }
                }

        Automatic compression of CSS for faster website

            打包後的CSS檔案大小較小，對將要讀取的Client速度會更快，而Sass則是在實現這一塊而產生。

            Sass可以設定為每次儲存檔案後馬上將sass編譯為css，方便web立即的讀取與應用。

        .. note ::

            Placeholder with CSS，HTML5中的標籤屬性，範例如下：

        Placeholder example

        .. code-block :: html

            <!DOCTYPE HTML>
            <html>
            <head></head>
                <body>
                    <input type='text' placeholder='aaaaaa'>
                </body>
            </html>


        What is Sass?

            Sass 是一種較為易讀與乾淨的標記性語言，來實現CSS的各中功能，來創建可管理的樣式表。

            .. note ::

                Sass 幕前依舊支持兩種類型，一種類型是基本的原生語法，他消除了我們css中的大括號，
                更多的資訊可以在http://sass-lang.com/docs/yardoc/file.INDENTED_SYNTAX.html中找到。

                而令一種則是本書中的語法，SCSS語法。

        What is Compass?

            Compass 是第一個 Sass-based 的框架，協助我們更方便輕鬆的建立CSS檔案。

            Compass 的git address:

                https://github.com/chriseppstein/compass

Installing Sass and Compass
"""""""""""""""""""""""""""""

    ::

        $ sudo apt-get install gem ruby

        $ sudo gem install sass compass

        ##check sass and compass version

        $ sass -v

        $ compass -v

        ## 自動編譯sass檔案

        $ compass watch

Summary
"""""""""""

    了解基本的Sasss與Compass 的用法，和如何安裝Sass and Compass

Chapter 2.
^^^^^^^^^^^^^^

Creating Compass projects
""""""""""""""""""""""""""

    如何建立一個compass專案，並且介紹config.rb的相關設定值與其效果。

    建立一個最基本的compass project

    ::

        $ compass create

    建立後的樹狀圖

    .. note ::
        .
        ├── config.rb
        ├── sass
        │   ├── ie.scss
        │   ├── print.scss
        │   └── screen.scss
        └── stylesheets

            ├── ie.css

                ├── print.css

                    └── screen.css

    .. note ::

        建立專案時compass不會自動建立javascript的檔案，所以請自己下指令建立。

    建立一個最乾淨的compass project

    ::

        $ compass create --bare --sass-dir "sass" --css-dir "css" --javascripts-dir "js" --images-dir "img"

    建立完成後的檔案結構為：

        .
        ├── config.rb
        └── sass

    關於config.rb

        ::

            # Require any additional compass plugins here.
            # 增加專案的plugins
            # ex :
            require "suzy"

            # 專案的基本設定，路徑 css img javascript的儲存位置
            http_path = "/" #http 的首頁路徑
            css_dir = "css" #css的儲存路徑
            sass_dir = "sass" #scss的儲存路徑
            images_dir = "img" #圖片的儲存路徑
            javascripts_dir = "js" #javascript 的儲存路徑

            # 設定文字的路徑
            fonts_dir="css/fonts"

            # 你可以選擇你的輸出格式，設定值有:expanded or :nested 或 :compact 或 :compressed

            # To enable relative paths to assets via compass helper functions. Uncomment:
            # ??測試過目前還看不出來哪裡有不一樣
            # relative_assets = true

            # 是否需要註解
            # line_comments = false
            # If you prefer the indented syntax, you might want to regenerate this
            # project again passing --syntax sass, or you can uncomment this:
            # preferred_syntax = :sass
            # and then run:
            # sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass

    config.rb(output_style=expanded)

    ::

        http_path = "/"
        css_dir = "css"
        sass_dir = "sass"
        images_dir = "img"
        javascripts_dir = "js"
        output_style = :expanded

    sass/test.scss

    .. code-block :: sass

        #main{
            color:#999;
            .content{
                color:#888;
            }
        }

    輸出的css/test.css

    .. code-block :: css

        /* line 1, ../sass/test.scss * /
        #main {
          color: #999;
        }
        /* line 3, ../sass/test.scss * /
        #main .content {
            color: #888;
        }

    config.rb(output_style=nested)

    ::

        http_path = "/"
        css_dir = "css"
        sass_dir = "sass"
        images_dir = "img"
        javascripts_dir = "js"
        output_style = :nested

    sass/test.scss

    .. code-block :: sass

        #main{
            color:#999;
            .content{
                color:#888;
            }
        }


    css/test.css

    .. code-block :: css

        /* line 1, ../sass/test.scss * /
        #main {
          color: #999; }
        /* line 3, ../sass/test.scss * /
        #main .content {
            color: #888; }

    config.rb

    ::

        http_path = "/"
        css_dir = "css"
        sass_dir = "sass"
        images_dir = "img"
        javascripts_dir = "js"
        output_style = :compact

    sass/test.scss

    .. code-block :: sass

        #main{
            color:#999;
            .content{
                color:#888;
            }
        }

    css/test.css

    .. code-block :: css

        /* line 1, ../sass/test.scss * /
        #main { color: #999; }
        /* line 3, ../sass/test.scss * /
        #main .content { color: #888; }

    config.rb

    ::

        http_path = "/"
        css_dir = "css"
        sass_dir = "sass"
        images_dir = "img"
        javascripts_dir = "js"
        output_style = :compact

    sass/test.scss

    .. code-block :: sass

        #main{
            color:#999;
            .content{
                color:#888;
            }
        }

    css/test.css

    .. code-block :: css

        #main { color: #999; }
        #main .content { color: #888; }

    config.rb

    ::

        http_path = "/"
        css_dir = "css"
        sass_dir = "sass"
        images_dir = "img"
        javascripts_dir = "js"
        output_style = :compressed

    sass/test.scss

    .. code-block :: sass

        #main{
            color:#999;
            .content{
                color:#888;
            }
        }

    css/test.css

    .. code-block :: css

        #main{color:#999}#main .content{color:#888}

Creating and using Partial files
"""""""""""""""""""""""""""""""""

    scss 檔案的import 引用

    sass/test.scss

    .. code-block :: sass

        #main{
        color:#999;
        .content{
            color:#888;
            background-image: url('../img/image.jpg');
            }
        }

    sass/partial.scss

    .. code-block :: sass

         @import "test.scss"

    output css

    .. code-block :: css

         #main {
           color: #999; }
        #main .content {
            color: #888;
            background-image: url("../img/image.jpg"); }

Sass comment formats
"""""""""""""""""""""""

    scss 的註解方式有以下幾種

    .. code-block :: scss

        /\*一般最普通的註解方式 \*/

        //單行註解模式

        //=================
        //第1層的註解
        //================

            //第2層的註解
            //============

        // loud 註解？？(還不知道再幹嘛的)
        // 只有說是最好的註解方式資料待查
        /\*! Remember me, I'm loud! \*/

總結
"""""""""""""""""""""""

    在這個章節我們學會了如何import 其他scss檔案，切割檔案並且管理是很重要的，
    如何建立一個compass專案另外以及如何使用註解讓你的檔案更加容易閱讀，並且讓
    我們更容易與其他人一起工作。

Chapter 3.
^^^^^^^^^^^^^^^^

    我們在這個章節中會了解的事情：

        * 什麼是Nesting?如何是用Nesting進行模塊化？

        * 如何使用@extend 允許我們擴大現有規則？

        * 在我們需要的時候使用placeholder selectors 風格

        * 什麼是Mixins?如何利用Mixins來減少重複性代碼？

        * 如何使用一個Mixins來達到我們需要的目的

    index.html

    .. code-block :: html

        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <title></title>
            </head>
            <body>
                <a class='test' href='#'>test</a>
            </body>
        </html>

    .. code-block :: sass

        a {
            color: red;
            &:hover,&:focus {
                color: blue;
            }
            &:visited,&:active {
                color: green;
            }
        }

    可以針對滑鼠的動作設定要顯示的css，編譯後的css為

    .. code-block :: css

        a {
            color: red;
        }

        a:hover, a:focus {
            color: blue;
        }

        a:visited, a:active {
            color: green;
        }


    每一個內嵌的sass 會依照每一個階層的結構去做編譯，範例如下：

    .. code-block :: sass

        .main {
          .content {
            width: 70%;
          }
          .content & {
            width: 100%;
          }
          .content & {
            .two & {
              color: pink;
            }
          }
        }

    編譯後

    .. code-block :: css

        .main .content {
          width: 70%;
        }
        .content .main {
          width: 100%;
        }
        .two .content .main {
            color: pink;
        }

    可以同時選擇兩個不同的class定義同一個css，範例如下：

    .. code-block :: sass

        .selector-one {
          &.selector-two {
            color: green;
          }
        }

    編譯後

    .. code-block :: css

        .selector-one.selector-two {
          color: green;
        }

    nesting 階層太多時，容易產生很多地雷，範例如下：

    .. code-block :: sass

        .nesting {
          .class {
            width: 100%;
          }
          #id {
            width: 100%;
          }
          &:hover {
            color: $color7;
          }
          .multi-nesting {
            .class-within {
              width: 90%;
              #id_within {
                width: 90%;
                &:hover {
                  color: $color4;
                }
              }
            }
          }
        }

    編譯後

    .. code-block :: css

        .nesting .class {
            width: 100%;
        }
        .nesting #id {
          width: 100%;
        }
        .nesting:hover {
          color: blue;
        }
        .nesting .multi-nesting .class-within {
          width: 90%;
        }
        .nesting .multi-nesting .class-within #id_within {
          width: 90%;
        }
        .nesting .multi-nesting .class-within #id_within:hover {
          color: chartreuse;
        }

    由於太多層的css 過於複雜，最好在設計的時候針對id或是class之下的相關標籤進行css 設定。

    example

    .. code-block :: sass

        #id_test {
            width: 90%;
            &:hover {
                color: red;
            }
        }

    使用id命名時必須要先考慮的幾個問題：

        * id 是一個頁面唯一的存在，是否會有重複的問題

        * 若一個標籤同時有class與id 的存在，是會以哪一個為優先？

        * 參考文獻：`http://benfra.in/1yq`

    .. note ::

        使用id標記方法有兩種

            * [id=:"id_name"] {}

            * #id_name {}

        Q:

            * 這兩種標注方法是否有不一樣？

            * 何者為佳？抑或兩種都可？

Nesting namespaces
"""""""""""""""""""""

    sass 有命名空間提供任何CSS屬性，

    example

    .. code-block :: sass

        .nesting-namespace-properties {
            border: {
                top: 1px dashed red;
                right: 1px dotted blue;
                bottom: 2px solid green;
                left: 1px solid black;
            }
        }

    編譯後

    .. code-block :: css

        .nesting-namespace-properties {
            border-top: 1px dashed red;
            border-right: 1px dotted blue;
            border-bottom: 2px solid green;
            border-left: 1px solid black;
        }

    example2

    namespace.html

    .. code-block :: html

        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <link rel="stylesheet" href="css/namespace.css">
                <title></title>
            </head>
            <body>
                <aside class="testmonial">test</aside>
                <blockquote>blockquote</blockquote>
            </body>
        </html>

    namespace.scss

    ..code-block :: sass

        .testimonial {

          background-color: #eee;
          border-top: 2px solid #ddd;
          border-bottom: 2px solid #ddd;
          padding: 1em 2em;

          > img {

            display: inline-block;
            width: 17%;

          }

          blockquote {

            margin: 0 0 0 3%;
            padding: 0 .5em;
            width: 76%;
            font-family: $blockquote;
            font-size: 2.4em;
            font-style:italic;
            display: inline-block;
            vertical-align: top;
            footer {

              font-size: .6em;
              border-top: 2px dotted #ddd;
              margin: 20px 0 0 0;
              padding: 10px 0 0 0;

            }

          }

          blockquote.small {

            font-size: .9em;

          }


    編譯後

    .. code-block :: css

        timonial {

          background-color: #eee;
          border-top: 2px solid #ddd;
          border-bottom: 2px solid #ddd;
          padding: 1em 2em;

        }

        timonial > img {

          display: inline-block;
          width: 17%;

        }

        timonial blockquote {

          margin: 0 0 0 3%;
          padding: 0 .5em;
          width: 76%;
          font-size: 2.4em;
          font-style: italic;
          display: inline-block;
          vertical-align: top;

        }

        timonial blockquote footer {

          font-size: .6em;
          border-top: 2px dotted #ddd;
          margin: 20px 0 0 0;
          padding: 10px 0 0 0;

        }

        timonial blockquote.small {

          font-size: .9em;

        }

@extend
""""""""""""

    @extend 可以將已經存在的class 或 id 或是標籤的css直接引入使用(繼承？)，範例如下

    extend.scss

    .. code-block :: sass

        .box {
            padding: 2em;
            color: red;
            background-color: #CCC;
        }

        .warning-box {
            @extend .box;
            border: 2px dotted red;
        }

        .success-box {
            @extend .box;
            border: 2px dotted green;
        }

        .info-box {
            @extend .box;
            border: 2px dotted blue;
        }

    編譯後

    .. code-block :: css

        .box, .warning-box, .success-box, .info-box {
          padding: 2em;
          color: red;
          background-color: #CCC;
        }

        .warning-box {
          border: 2px dotted red;
        }

        .success-box {
          border: 2px dotted green;
        }

        .info-box {
          border: 2px dotted blue;
        }

    sass 可以將@extend的各個class, id 做group的動作，然後在對各別的css做宣告，如範例中warning-box,
    success-box, info-box都有.box的共同屬性，所以使用@extend .box會將所有的box類別進行group，然後再
    依照warning, success, info進行宣告。

    extend.scss

    .. code-block :: sass

        %box {
            padding: 2em;
            color: red;
            background-color: #CCC;
        }

        .warning-box {
            @extend .box;
            border: 2px dotted red;
        }

        .success-box {
            @extend .box;
            border: 2px dotted green;
        }

        .info-box {
            @extend .box;
            border: 2px dotted blue;
        }

    編譯後

    .. code-block :: css

        .warning-box, .success-box, .info-box {
          padding: 2em;
          color: red;
          background-color: #CCC;
        }

        .warning-box {
          border: 2px dotted red;
        }

        .success-box {
          border: 2px dotted green;
        }

        .info-box {
          border: 2px dotted blue;
        }

    .. note ::

        .box 與 %box的差別在於編譯後.box這個class 仍然會存在於css之中，而若是%box則會將各個
        warning-box, success-box, info-box 做group的動作而掠過box。主要的原因在於避免產生多餘
        的css規則。

Mixins
"""""""""""""

    example

    _layout.scss

    .. code-block :: sass

        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

    .. note ::

        The star selector was used to select all elements and change the box-sizing model to border-box.
        To get the best browser support possible, the property was repeated a number of times with vendor
        prefixes (one for WebKit and another for Mozilla with the final official property and value pair
        listed last so that it supersedes the others, if present)

        \* 選擇器將所有的box-sizing model 修改為 border-box以獲得最佳的支援。

        Q:

            * 什麼是 \* 選擇器？

            * 什麼是border-box?

                Microsoft 從 IE8就開始實行border-box。
                參考文獻：http://paulirish.com/2012/box-sizing-border-box-ftw/

