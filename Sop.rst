.. sphinx documentation master file, created by
   sphinx-quickstart on Wed Apr 10 10:30:15 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
Sop
==================================

.. toctree::
    :glob:

    sop/node-webkit
    sop/user-login
    sop/flask-openid
