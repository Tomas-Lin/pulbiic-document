Eve mongo class trace
----------------------------------------------------------------------

    ::
    
        $ git clone https://github.com/nicolaiarocci/eve.git
        
    git 後的部份架構，只留下這次我有trace的部份。
    
    ::
    
        .
        └── eve
            ├── eve
            │   ├── __init__.py
            │   ├── io
            │   │   ├── base.py
            │   │   ├── __init__.py
            │   │   └── mongo
            │   │       ├── __init__.py
            │   │       ├── mongo.py
            │   │       ├── parser.py
            │   │       └── validation.py
            │   ├── render.py
            │   ├── utils.py
            │   └── validation.py
            ├── LICENSE
            ├── MANIFEST.in
            ├── README.rst
            ├── requirements.txt
            └── setup.py
            
    使用的是flask-pymongo，作MongoDB的控制與設定。
    
    mongo.py作MongoDB的CRUD
    
    schema 比對使用 cerberus modules 實現
    
    github : https://github.com/nicolaiarocci/cerberus
    
    code-block :: python
    
        import cerberus
        v = Validator({'name': {'type': 'string'}})
        print v.validate({'name': 'john doe'})          #True
        print v.validate({'name': 'eee', 'aaa':'ddd'})  #False
        v.validate_update({'name': 'string', 'aaa':'string'}, {'name': {'type': 'string'}, 'aaa': {'type': 'string'}})
        print v.validate({'name': 'eee', 'aaa':'ddd'})  #True
        
    
cerberus
---------------------------------------------------------------

    支援Python2.6 Python2.7 Pyhton3.3版本
    
    比對dict的type與其他rule
    
    simple demo:
    
    .. code-block :: python
        
        schema = {'name': {'type': 'string'}}
        v = Validator(schema)
        document = {'name': 'john doe'}
        v.validate(document)    # True
    
    或是
    
    .. code-block :: python
    
        v = Validator()
        v.validate(document, schema)    # True
        
Non-Blocking
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    Cerberus 不像其他的module，會立即的傳回錯誤訊息，而會將錯誤訊息儲存於self.error中，
    這是一個list。
    
    demo
    
    .. code-block :: python
    
        schema = {'name': {'type': 'string'}, 'age': {'type': 'integer', 'min': 10}}
        document = {'name': 1337, 'age': 5}
        v.validate(document, schema)    #False
        print v.errors   #["min value for field 'age' is 10", "value of field 'name' must be of string type"]
        
    你也會收到schemaError和ValidationError exceptions
    
Allowing the unknown
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    只有default的schema可以回傳True，若是有其他未知的schema則會回傳False。
    
    demo
    
    .. code-block :: python
    
        schema = {'name': {'type': 'string', 'maxlength': 10}}
        v.validate({'name': 'john', 'sex': 'M'})    #False
        print v.errors  # ["unknown field 'sex'"]
    
    How to allow the unknow field?
    
    .. code-block :: python
    
        schema = {'name': {'type': 'string', 'maxlength': 10}}
        v.allow_unknown = True
        v.validate({'name': 'john', 'sex': 'M'})    #True
        
        or
        
        v = Validator(schema=schema, allow_unknown=True)
        v.validate({'name': 'john', 'sex': 'M'})    #True
        
Custom the validators
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    Cerberus 可以更的讓你客製化自己需要的schema驗證。
    
    Demo
    
    .. code-block :: python
    
        schema = {'oddity': {'isodd': True, 'type': 'integer'}}
        from cerberus import Validator
        class MyValidator(Validator):
            def _validate_isodd(self, isodd, field, value):
                if isodd and not bool(value & 1):
                    self._error("Value for field '%s' must be an odd number" % field)
        
        v = MyValidator(schema)
        v.validate({'oddity': 10})  # False
        print v.errors    #['Value for field 'oddity' must be an odd number']
        v.validate({'oddity': 9})   # True
    
    可以新增data type，
    
    Demo
    
    .. code-block :: python
    
        def _validate_type_objectid(self, field, value):
        if not re.match('[a-f0-9]{24}', value):
           print self._error(ERROR_BAD_TYPE % (field, 'ObjectId'))
    
Type
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    Data type 有下列幾個項目：
    
        string
        
        integer
        
        float
        
        boolean
        
        datetime
        
        dict
        
        list
    
    **required** : 
    
        當值為True時，執行 validate()所收到的回傳值會是False，但是 validate_update() 回傳質則為True。
        
    Demo
    
    .. code-block :: python
    
        schema = {'name': {'required': True, 'type': 'string'}, 'age': {'type': 'integer'}}
        v = Validator(schema)
        document = {'age': 10}
        v.validate(document)    #False
        v.errors    #["required field(s) are missing: 'name'"]

        v.validate_update(document) #True
    
    .. note ::
    
        String fields with empty values will still be validated, even when required is set to True. If you don’t want to accept empty values, see the empty rule.
    
    **readonly**
    
        若該值為Ture，Validation will fail if this field is present in the target dictionary.
        
    **nullable**
    
        若為True，則該欄位的值可以設定為None
        
        Demo
        
        code-block :: python
        
            schema = {'a_nullable_integer': {'nullable': True, 'type': 'integer'}, 'an_ingeger': {'type': 'integer'}}
            v = Validator(schema)

            print v.validate({'a_nullable_integer': 3})   #True
            print v.validate({'a_nullable_integer': None})    #True
            print v.validate({'an_integer': 3})       #True
            print v.validate({'an_integer': None})    #False
            print v.errors    #["value of field 'an_integer' must be of integer type"]
            
    **minlength, maxlength**
    
        string和list才有這個設定，最大與最小長度設定
        
    **min, max**
    
        int才有此設定，最大與最小值
        
    **allowed**
    
        list才有此設定，設定預設值，若是list中若是預設值回傳True，反之則回傳False
        
        Demo
        
        .. code-block :: python
        
            schema = {'role': {'type': 'list', 'allowed': ['agent', 'client', 'supplier']}}
            v = Validator(schema)
            print v.validate({'role': ['agent', 'supplier']})   #True

            print v.validate({'role': ['intern']})  #False
            print v.errors  #["unallowed values ['intern'] for field 'role'"]

            schema = {'role': {'type': 'string', 'allowed': ['agent', 'client', 'supplier']}}
            v = Validator(schema)
            print v.validate({'role': 'supplier'})    #True

            v.validate({'role': 'intern'})  #False
            print v.errors  #["unallowed value 'intern' for field 'role'"]
            
    **empty**
    
        只有string才有此設定，若設定為False則字串不能為空，
        
        Demo
        
        .. code-block :: python
        
            schema = {'name': {'type': 'string', 'empty': False}}
            document = {'name': ''}
            print v.validate(document, schema)  #False

            print v.errors  #["empty values not allowed for field 'name'"]
            
    **items(dict)**
    
        定義一個在list中的dict的類型
        
        Demo
        
        .. code-block :: python
        
            schema = {'rows': {'type': 'list', 'items': {'sku': {'type': 'string'}, 'price': {'type': 'integer'}}}}
            document = {'rows': [{'sku': 'KT123', 'price': 100}]}
            print v.validate(document, schema)  #True
            
    **items(list)**
    
        定義一個list allowed in a list type of fixed length:
        
        Demo
        
        .. code-block :: python
        
            schema = {'list_of_values': {'type': 'list', 'items': [{'type': 'string'}, {'type': 'integer'}]}}
            document = {'list_of_values': ['hello', 100]}
            print v.validate(document, schema)  #True
    
    **schema**
        
        只有 dict與 list 的類別才有的設定
        
        在dict Demo
        
        .. code-block :: python
        
            schema = {'a_dict': {'type': 'dict', 'schema': {'address': {'type': 'string'}, 'city': {'type': 'string', 'required': True}}}}
            document = {'a_dict': {'address': 'my address', 'city': 'my town'}}
            print v.validate(document, schema)  #True
            
        list Demo
        
        .. code-block :: python
        
            schema = {'a_list': {'type': 'list', 'schema': {'type': 'integer'}}}
            document = {'a_list': [3, 4, 5]}
            print v.validate(document, schema)  #True
            
        在定義list的時候schema是最常用的設定 Demo
        
        .. code-block :: python
        
            schema = {'rows': {'type': 'list', 'schema': {'type': 'dict', 'schema': {'sku': {'type': 'string'}, 'price': {'type': 'integer'}}}}}
            document = {'rows': [{'sku': 'KT123', 'price': 100}]}
            print v.validate(document, schema)  #True
            
        陣列包物件的Sample
        
        .. code-block :: python
            
            #schema
            
            {
                "questions":{
                    "type":"list",
                    "schema":{
                        "type":"dict",
                        "schema":{
                            "mysql_q_id":{"type":"string"},
                        }
                    }
                }
            }
            
            #data
            {
                "questions": [
                    {
                        "mysql_q_id": "141",
                    }
                ]
            }
            
        物件包陣列的Sample
        
        .. code-block :: python
            
            #Schema
            
            {
                "opt_ans":{
                    "type":"list","maxlength":5
                }
            }
            
            #data
            
            {
                "opt_ans":["A"]
            }
            
Validator Class
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    class cerberus.Validator(schema=None, transparent_schema_rules=False, ignore_none_values=False, allow_unknown=False)
    
        Validator class. Validates any Python dict against a validation schema, which is provided as an argument at class instantiation, or upon calling the validate() or validate_update() methods.
            Parameters: 
            
            schema – optional validation schema.
            
            transparent_schema_rules – if True unknown schema rules will be ignored (no SchemaError will be raised). Defaults to False. Useful you need to extend the schema grammar beyond Cerberus’ domain.
            ignore_none_values – If True it will ignore None values for type checking. (no UnknowType error will be added). Defaults to False. Useful if your document is composed from function kwargs with defaults.
            allow_unknown – if True unknown key/value pairs (not present in the schema) will be ignored, and validation will pass. Defaults to False, returning an ‘unknown field error’ un validation.
        
        errors
        
            Return type:    a list of validation errors. Will be empty if no errors were found during. Resets after each call to validate() or validate_update().
            
        validate(document, schema=None)
        
            Validates a Python dictionary against a validation schema.

            Parameters: 
            document – the dict to validate.
            
            schema – the validation schema. Defaults to None. If not provided here, the schema must have been provided at class instantation.
            
            Returns:    
            True if validation succeeds, False otherwise. Check the errors() property for a list of validation errors.
            
            validate_update(document, schema=None)
            Validates a Python dicitionary against a validation schema. The difference with validate() is that the required rule will be ignored here.

            Parameters: schema – optional validation schema. Defaults to None. If not provided here, the schema must have been provided at class instantation.
            Returns:    True if validation succeeds, False otherwise. Check the errors() property for a list of validation errors.
            
    Exceptions
    
        class cerberus.SchemaError
        
            Raised when the validation schema is missing, has the wrong format or contains errors.
            
        class cerberus.ValidationError
    
            Raised when the target dictionary is missing or has the wrong format
        
參考 source code
---------------------------------------------------------------------

    git : https://github.com/nicolaiarocci/eve
    
    document :  http://cerberus.readthedocs.org/en/latest/
