Restful
----------------------------------------------------

Features
""""""""""""""""""""""""""""""""""""""""""""""""""""

    eve 支援 MongoDB的CRUD。
    
    +-------+-----------+-------------------------+
    |Action | HTTP Verb | Context                 |
    +-------+-----------+-------------------------+
    |Create | GET, HEAD | Collection, Document    |
    +-------+-----------+-------------------------+
    |READ   | GET, HEAD | Collection, Document    |
    +-------+-----------+-------------------------+
    |Update |PATCH      | Document                |
    +-------+-----------+-------------------------+
    |Delete |DELETE     | Collection/Document     |
    +-------+-----------+-------------------------+
    
    simple demo
    
    run.py
    
    .. code-block:: python
    
        from eve import Eve

        if __name__ == '__main__':
            app = Eve()
            app.run()
            
    setting.py
    
    .. code-block:: python
    
        people={}
        schema={}
            
    需要run.py與setting.py兩個檔案，setting.py檔案名稱為預設值，schema為MongoDB的欄位預設值，
    people為網址
    
Quick Start
""""""""""""""""""""""""""""""""""""""""""""""""""""
    
    run.py
    
    .. code-block:: python
        
        from eve import Eve

        if __name__ == '__main__':
            app = Eve()
            app.run()
            
    settings.py
    
    .. code-block:: python
    
        SERVER_NAME = '127.0.0.1:5000'
        DOMAIN={
            'people':{}
        }

Database Interlude
"""""""""""""""""""""""""""""""""""""""""""""""""""""

    連線到MongoDB
    
    settings.py
    
    .. code-block:: python
        
        SERVER_NAME = '127.0.0.1:5000'
        
        DOMAIN={
        
            'people':{}
            
        }
    
        MONGO_HOST='localhost'
        
        MONGO_PORT=27017
        
        MONGO_DBNAME='apitest'

A More Complex Application
"""""""""""""""""""""""""""""""""""""""""""""""""""""

    Complet Demo 
    
    
    run.py
    
    .. code-block:: python
    
        from eve import Eve

        if __name__ == '__main__':
        
            app = Eve()
            
            app.run()
            
    settings.py sample
    
    .. code-block:: python
        
        MONGO_HOST='localhost'
        
        MONGO_PORT=27017
        
        MONGO_DBNAME='apitest'
        
        RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
        
        ITEM_METHODS = ['GET', 'PATCH', 'DELETE']
        
        schema = {
        
            'firstname': {
            
                'type': 'string',
                
                'minlength': 1,
                
                'maxlength': 10,
                
            },
            
            'lastname': {
            
                'type': 'string',
                
                'minlength': 1,
                
                'maxlength': 15,
                
                'required': True,
                
                'unique': True,
                
            },
            
            'role': {
            
                'type': 'list',
                
                'allowed': ["author", "contributor", "copy"],
                
            },
            
            'location': {
            
                'type': 'dict',
                
                'schema': {
                
                    'address': {'type': 'string'},
                    
                    'city': {'type': 'string'}
                    
                },
                
            },
            
            'born': {
            
                'type': 'datetime',
                
            },
            
        }
        
        people = {
        
            'item_title': 'person',
            
            'additional_lookup': {
            
                'url': '[\w]+',
                
                'field': 'lastname'
                
            },
            
            'cache_control': 'max-age=10,must-revalidate',
            
            'cache_expires': 10,
            
            'resource_methods': ['GET', 'POST'],
            
            'schema': schema
            
        }

        SERVER_NAME = '127.0.0.1:5000'
        
        DOMAIN={
        
            'people':people
            
        }
        

Configuration Files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    設定檔為settings.py
    
    可以下參數修改檔案名稱：
    
     run.py
     
    .. code-block:: python
        
        from eve import Eve

        if __name__ == '__main__':
        
            app = Eve(setting='mysetting.py')
            
            app.run()
    
    若是多個檔案可以在settings.py中利用EVE_SETTINGS變數來作指定
    
    
    
參考資料
--------------------------------------------------------------------

    光頭DEMO:http://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask
    
