Basic_demo
---------------------------------------------------------
    map function

    .. code-block :: python

        def cube(x):

            return x*x*x

        l = map(cube, range(1, 11))
        執行結果：
        [1, 8, 27, 64, 125, 216, 343, 512, 729, 1000]


    Read Json

    .. code-block :: python

        from json import loads
        json_file = open('math.json', 'r')
        DB_data = loads(json_file.read())

    正規表示法re

    .. code-block :: python

        import re
        str='設x，y，z為自然數，若(x，y) = 21，(y，z) = 56，則(x，y，z)之值為 (1) 1　(2) 7　(3) 9　(4) 21 (5) 28'
        split = re.split('\([1-9]\)', aaa)  #['設x，y，z為自然數，若(x，y) = 21，(y，z) = 56，則(x，y，z)之值為', u'1', u'7', u'9', u'21', u'28']

    陣列刪除某個元素

    .. code-block :: python

        a = [1, 2, 3]
        a.pop(0)
        print a  # [2, 3]
        a.pop(1)
        print a # [2]

    strip是trim掉字符串两邊的空格。
    lstrip, trim掉左邊的空格
    rstrip, trim掉右邊的空格

    .. code-block :: python

        arr = [u' regularly ', u' directly ', u' hardly ', u' gradually']
        for i in range(0, len(arr)):
            arr[i]=arr[i].strip()
        print arr # [u'regularly', u'directly', u'hardly', u'gradually']

    raise 資料來源： http://docs.python.org/2/tutorial/errors.html#raising-exceptions

    .. code-block :: python

        try:
            raise NameError('HiThere')
        except NameError:
            print 'An exception flew by!'
            raise

    isinstance :  what different with types

    .. code-block :: python

        import types

        class Vehicle:
            pass

        class Truck(Vehicle):
            pass


    yield:

    Demo

    .. code-block :: python

        def simple_generator_function():
            yield 1
            yield 2
            yield 3


        # 1
        # 2
        # 3

        our_generator = simple_generator_function()

        print next(our_generator)
        print next(our_generator)
        print next(our_generator)

        # 1
        # 2
        # 3

    陣列 and 小技巧

    Demo

    .. code-block :: python

        a = [2,3,4,5,6,7,8,9,0]
        xyz = [0,12,4,6,242,7,9]
        for x in filter(lambda w: w in a, xyz):
          print x

    list print index and value

    Demo

    .. code-block :: python

        q = ["a","b","c","d","e"]
        for index, value in enumerate(q):
            print dict({index:value})


        """
        display:

        {0: 'a'}
        {1: 'b'}
        {2: 'c'}
        {3: 'd'}
        {4: 'e'}

        """

    string to dictionary

    DEMO

    .. code-block :: python

        import ast
        ast.literal_eval("{'muffin' : 'lolz', 'foo' : 'kitty'}")
        {'muffin': 'lolz', 'foo': 'kitty'}

    確認檔案是否存在

    DEMO

    .. code-block :: python

        from os import path
        if not path.exists(fileName):
            f = open('fileName', 'w')
            f.write('Hello World!')
            f.close()

    停止一段時間後繼續執行

    DEMO

    .. code-block :: python

        import time
        print '開始'
        time.sleep(5)
        print '過了五秒'

    檢查資料夾是否存在，若是不存在則產生資料夾

    DEMO

    .. code-block :: python

        import os
        path = "test_dir"
        if not os.path.isdir(path):
            os.mkdir(path)

    list replace 元素

    .. code-block :: python

        #words = ['how', 'much', 'is[br]', 'the', 'fish[br]', 'no', 'really'] 來源

        #words = ['how', 'much', 'is<br />', 'the', 'fish<br />', 'no', 'really'] 結果

        words = [w.replace('[br]', '<br />') for w in words]

class special method
-----------------------

    * __init__ : initial 函數，定義藉由class產生的object 的初始值。

    * __call__ : Demo

    .. code-block :: python

        #-*- coding:utf-8 -*-
        class Test1(object):
            def __init__(self, name):
                self.name = name

            def __call__(self):
                print 'My name is {}'.format(self.name)

        class Test2(object):
            def __init__(self, name):
                self.name=name


        Tomas = Test1('Tomas!')
        Alan = Test2('Alan!')

        print '執行 Tomas()'
        Tomas()
        print '執行 Alan()'
        Alan()

    .. note ::

        執行結果：

            執行 Tomas()
            My name is Tomas!
            執行 Alan()
            Traceback (most recent call last):
            File "class_special_method_demo.py", line 20, in <module>
            Alan()
            TypeError: 'Test2' object is not callable

    * __str__ : DEMO

    * __repr__ : demo

        .. code-block :: python

            #-*- coding:utf-8 -*-
            #__str__ demo
            class A(object):
                def __init__(self, name):
                    self.name = name
                def __str__(self):
                    return "I'm {}".format(self.name)
            class B(object):
                def __init__(self, name):
                    self.name=name

                def __repr__(self):
                    return "I'm {}".format(self.name)


            Tomas = A('Tomas')
            Alan = B('Alan')
            print 'print Tomas'
            print Tomas
            print 'print Alan'
            print Alan

        .. note ::

            若__repr__被定義，而__str__沒有被定義的話，object會默認為__repr__=__str__

            __repr__ 和__str__有何不同？

            若__repr__和__str__2__都沒有定義，會回傳該object的記憶體位置
